<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['defaultinstitution'] = 'Lehenetsitako erakundea';
$string['description'] = 'Autentifikatu SAML 2.0 IdP zerbitzu batean';
$string['errnosamluser'] = 'Ez da erabiltzailerik aurkitu';
$string['errorbadcombo'] = 'Urrutiko erabiltzailea aukeratu ez baduzu, erabiltzailea automatikoki sortzea baino ezin duzu aukeratu';
$string['errorbadconfig'] = '%s SimpleSAMLPHP konfigurazio-direktorioa ez da zuzena.';
$string['errorbadinstitution'] = 'Erabiltzailea konektatzeko erakudendea ez da ebatzi';
$string['errorbadlib'] = '%s SimpleSAMLPHP lib direktorioa ez da zuzena.';
$string['errormissinguserattributes'] = 'Autentikatuta zaudela dirudien arren, ez ditugu jaso behar diren erabiltzaile-ezaugarriak. Mesedez, zure Nortasun Hornitzaileak Izena eta Deituraren SSO eremuak bidaltzen dituela eta Mahara Zerbitzu Hornitzailearen Emaila badabilela egiaztatu, edo zerbitzari honetako webmasterrari jakinarazi.';
$string['errorretryexceeded'] = 'Gehieneko saiakera-kopurua gainditu egin da (%s) - Identitate-zerbitzuak arazoren bat izan behar du';
$string['institutionattribute'] = 'Erakundearen atributuak ("%s" barne)';
$string['institutionregex'] = 'Erakundearen izen laburra katearekin bat dator partzialki';
$string['institutionvalue'] = 'Atributuarekin erkatzeko erakundearen balorea';
$string['notusable'] = 'Mesedez, instalatu SimpleSAMLPHP SP liburutegiak';
$string['remoteuser'] = 'Lotu erabiltzaile-izenaren atributua urrutiko erabiltzaile-izenari';
$string['samlfieldforemail'] = 'SSO eremua e-postarako';
$string['samlfieldforfirstname'] = 'SSO eremua izenerako';
$string['samlfieldforsurname'] = 'SSO eremua deiturarako';
$string['simplesamlphpconfig'] = 'SimpleSAMLPHP konfigurazio-direktorioa';
$string['simplesamlphplib'] = 'SimpleSAMLPHP lib direktorioa';
$string['title'] = 'SAML autentifikazioa';
$string['updateuserinfoonlogin'] = 'Eguneratu erabiltzailearen xehetasunak saioa hastean';
$string['userattribute'] = 'Erabiltzailearen atributua';
$string['weautocreateusers'] = 'Erabiltzaileak automatikoki sortzen ditugu';
?>
