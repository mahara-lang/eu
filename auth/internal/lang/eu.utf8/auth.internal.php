<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['completeregistration'] = 'Erregistro osoa';
$string['description'] = 'Egiaztatu Mahararen datu-basean';
$string['emailalreadytaken'] = 'Email-helbide hau dagoeneko erregistratuta dago';
$string['iagreetothetermsandconditions'] = 'Ados nago baldintzekin';
$string['internal'] = 'Barnekoa';
$string['passwordformdescription'] = 'Pasahitzak, gutxienez, 6 karaktereko luzera izan behar du, zenbaki bat eta hizki bi barne.';
$string['passwordinvalidform'] = 'Pasahitzak, gutxienez, 6 karaktereko luzera izan behar du, zenbaki bat eta hizki bi barne.';
$string['registeredemailmessagehtml'] = '<p>Kaixo %s,</p>
<p>Eskerrik asko kontua irekitzeagatik %s-(e)n. Mesedez, jarraitu ondoko esteka izenemate-prozesua osatzeko:</p>
<p><a href="%sregister.php?key=%s">%sregister.php?key=%s</a></p> <p>Esteka 24 orduan iraungiko da.</p>

<pre>--
Agur bero bat,
%s Taldea</pre>';
$string['registeredemailmessagetext'] = 'Kaixo %s,

Eskerrik asko kontua irekitzeagatik %s-(e)n. Mesedez, jarraitu ondoko esteka izenemate-prozesua osatzeko:

%sregister.php?key=%s

--
Agur bero bat,
 %s Taldea';
$string['registeredemailsubject'] = '%s-(e)n erregistratu zara';
$string['registeredok'] = '<p>Ondo erregistratu zara. Emailean aurkituko duzu zein urrats egin kontua aktibatzeko.</p>';
$string['registrationnosuchkey'] = 'Sentitzen dugu, ematen du ez dagoela konturik pasahitz honekin. 24 ordu baino gehiago daramazu erregistroa osatzeko zain? Horrela bada, gure akatsa izan daiteke.';
$string['registrationunsuccessful'] = 'Sentitzen dugu, zure erregistratzeko saiakerak huts egin du. Gure akatsa izan da, ez zurea. Saiatu berriro geroago.';
$string['title'] = 'Barrukoa';
$string['usernamealreadytaken'] = 'Sentitzen dugu, erabiltzaile-izen hori hartuta dago.';
$string['usernameinvalidadminform'] = 'Erabiltzaile-izenek hizkiak, zenbakiak eta sinbolo ohikoenak eduki ditzakete, eta 3 eta 236 karaktereren arteko luzera izan behar dute. Hutsunerik ez da onartzen.';
$string['usernameinvalidform'] = 'Zure erabiltzaileak karaktere alfanumerikoak, puntuak, azpiko marrak eta @ ikurra baino ezin ditu izan. Era berean, 3 eta 30 arteko karaktereko luzera izan behar du. Tarte hutsak ez dira onartzen.';
$string['youmaynotregisterwithouttandc'] = 'Ezin zara erregistratu <a href="terms.php">Baldintzak</a> onartu ezean.';
$string['youmustagreetothetermsandconditions'] = 'Onartu egin behar dituzu <a href="terms.php">Baldintzak</a>';
?>
