<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['Body'] = 'Gorputza';
$string['Close'] = 'Itxi';
$string['Closed'] = 'Itxita';
$string['Count'] = 'Kontatu';
$string['Key'] = 'Gakoa';
$string['Moderators'] = 'Moderatzaileak';
$string['Open'] = 'Ireki';
$string['Order'] = 'Agindua';
$string['Post'] = 'Bidali';
$string['Poster'] = 'Bidaltzailea';
$string['Posts'] = 'Mezuak';
$string['Reply'] = 'Erantzun';
$string['Sticky'] = 'Nabarmendu';
$string['Subject'] = 'Gaia';
$string['Subscribe'] = 'Harpidetu';
$string['Subscribed'] = 'Harpidetuta';
$string['Topic'] = 'Gaia';
$string['Topics'] = 'Gaiak';
$string['Unsticky'] = 'Ez nabarmendu';
$string['Unsubscribe'] = 'Eten harpidetza';
$string['activetopicsdescription'] = 'Zure taldeetan azkenaldian eguneratutako gaiak';
$string['addpostsuccess'] = 'Mezua ondo gehitu da';
$string['addtitle'] = 'Gehitu foroa';
$string['addtopic'] = 'Gehitu gaia';
$string['addtopicsuccess'] = 'Gaia ondo gehitu da';
$string['allposts'] = 'Mezu guztiak';
$string['autosubscribeusers'] = 'Erabiltzaileak automatikoki harpidetu?';
$string['autosubscribeusersdescription'] = 'Aukeratu talde-erabiltzaileak automatikoki harpidetuko diren foro honetara';
$string['cantaddposttoforum'] = 'Ezin duzu mezurik utzi foro honetan';
$string['cantaddposttotopic'] = 'Ezin duzu mezurik utzi gai honetan';
$string['cantaddtopic'] = 'Ezin dituzu gaiak gehitu foro honetan';
$string['cantdeletepost'] = 'Ezin dituzu mezuak ezabatu foro honetan';
$string['cantdeletethispost'] = 'Ezin duzu mezu hori ezabatu';
$string['cantdeletetopic'] = 'Ezind dituzu gaiak ezabatu foro honetan';
$string['canteditpost'] = 'Ezin duzu mezu hori editatu';
$string['cantedittopic'] = 'Ezin duzu gai hori editatu';
$string['cantfindforum'] = '%s id-a duen foroa ezin izan da aurkitu';
$string['cantfindpost'] = '%s id-a duen posta ezin izan da aurkitu';
$string['cantfindtopic'] = '%s id-a duen gaia ezin izan da aurkitu';
$string['cantviewforums'] = 'Ezin dituzu talde honetako foroak ikusi';
$string['cantviewtopic'] = 'Ezin dituzu foro honetako gaiak ikusi';
$string['chooseanaction'] = 'Aukera ezazu ekintza bat';
$string['clicksetsubject'] = 'Klikatu arlo bat jartzeko';
$string['closeddescription'] = 'Itxitako gaiei moderatzaileek eta taldearen jabeak baino ezin diete erantzun';
$string['closetopics'] = 'Itxi gai berriak';
$string['closetopicsdescription'] = 'Hau markatuta, foro honetako gai guztiak berez itxiko dira. Moderatzaile eta taldeko kudeatzaileek bakarrik erantzun ahal izango diete itxitako gaiei.';
$string['createtopicusersdescription'] = '&quot;Taldekide guztiak&quot; ezarriz gero, edozeinek sor ditzake gai berriak eta daudenei erantzun. &quot;Moderatzaileak eta talde-kudeatzaileak&quot; hautatuz gero, soilik moderatzaileek eta talde-administratzaileek sor ditzakete gai berriak, baina behin sortutakoan, erabiltzaile guztiek egin ditzakete iruzkinak.';
$string['currentmoderators'] = 'Oraingo moderatzaileak';
$string['defaultforumdescription'] = '%s eztabaida orokorrerako foroa';
$string['defaultforumtitle'] = 'Eztabaida orokorra';
$string['deleteforum'] = 'Ezabatu foroa';
$string['deletepost'] = 'Ezabatu mezua';
$string['deletepostsuccess'] = 'Posta ondo ezabatu da';
$string['deletepostsure'] = 'Ziur al zaude hau egin nahi duzula? Ezin izango da desegin.';
$string['deletetopic'] = 'Ezabatu gaia';
$string['deletetopicsuccess'] = 'Gaia ondo ezabatu da';
$string['deletetopicsure'] = 'Ziur al zaude hau egin nahi duzula? Ezin izango da desegin.';
$string['deletetopicvariable'] = 'Ezabatu \'%s\' gaia';
$string['editpost'] = 'Editatu mezua';
$string['editpostsuccess'] = 'Mezua ondo editatu da';
$string['editstothispost'] = 'Mezu honen edizioak:';
$string['edittitle'] = 'Editatu foroa';
$string['edittopic'] = 'Editatu gaia';
$string['edittopicsuccess'] = 'Gaia ondo editatu da';
$string['forumname'] = 'Foroaren izena';
$string['forumposthtmltemplate'] = 'div style="padding: 0.5em 0; border-bottom: 1px solid #999;"><strong>%s by %s</strong><br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p><a href="%s">Mezu honi lerroan erantzun</a></p> <p><a href="%s">%s(r)en harpidetza eten </a></p> </div>';
$string['forumposttemplate'] = '%s-(r)en %s %s ------------------------------------------------------------------------ %s ------------------------------------------------------------------------ Mezua online ikusi eta erantzuteko, klikatu esteka honetan: %s. %s-(e)ko harpidetza eteteko, joan hona: %s';
$string['forumsuccessfulsubscribe'] = 'Foroko harpidetza ondo egin da';
$string['forumsuccessfulunsubscribe'] = 'Foroko harpidetza ondo eten da';
$string['gotoforums'] = 'Joan foroetara';
$string['groupadminlist'] = 'Taldeko kudeatzaileak:';
$string['groupadmins'] = 'Taldeko kudeatzaileak';
$string['indentflatindent'] = 'Koskarik ez';
$string['indentfullindent'] = 'Zabaldu osorik';
$string['indentmaxindent'] = 'Zabaldu handienera';
$string['indentmode'] = 'Foroetako koskak';
$string['indentmodedescription'] = 'Zehaztu foro honetako gaiek zein koska izan behar duten.';
$string['lastpost'] = 'Azken mezua';
$string['latestforumposts'] = 'Foroko azken mezuak';
$string['maxindent'] = 'Gehienezko koska-maila';
$string['maxindentdescription'] = 'Zehaztu gai baterako koskatze-maila altuena. Hau bakarrik aplikatuko da koskatze-modua gehienera zabaltzeko ezarri bada';
$string['moderatorsandgroupadminsonly'] = 'Moderatzaileak eta talde-kudeatzaileak bakarrik';
$string['moderatorsdescription'] = 'Moderatzaileek gaiak eta mezuak editatu eta ezabatu ahal dituzte. Era berean, gaiak ireki, itxi, jarri eta kendu ahal dituzte.';
$string['moderatorslist'] = 'Moderatzaileak:';
$string['name'] = 'Foroa';
$string['nameplural'] = 'Foroak';
$string['newforum'] = 'Foro berria';
$string['newforumpostnotificationsubject'] = '%s: %s: %s';
$string['newpost'] = 'Mezu berria:';
$string['newtopic'] = 'Gai berria';
$string['noforumpostsyet'] = 'Talde honetan ez dago mezurik oraindik';
$string['noforums'] = 'Talde honetan ez dago fororik';
$string['notopics'] = 'Foro honetan ez dago gairik';
$string['orderdescription'] = 'Aukeratu non ordenatu nahi duzun foroa beste foroekin alderatuta';
$string['postaftertimeout'] = 'Zure aldaketa %s minutuko gehiegizko atzerapenaz bidali duzu. Aldaketak ez dira aplikatu.';
$string['postbyuserwasdeleted'] = '%(r)en mezua ezabatu egin da';
$string['postdelay'] = 'Bidali aurreko tartea';
$string['postdelaydescription'] = 'Zein denbora-tarte pasatu behar da (minututan) mezua foroko harpidedunei e-postaz bidaltzeko. Mezuaren egileak tarte horretan aldaketak egin ditzake.';
$string['postedin'] = '%s(e)k %s(e)n bidalita';
$string['postreply'] = 'Erantzun mezuari';
$string['postsvariable'] = 'Mezuak: %s';
$string['potentialmoderators'] = 'Balizko moderatzaileak';
$string['re'] = 'Er: %s';
$string['regulartopics'] = 'Ohiko gaiak';
$string['replyforumpostnotificationsubject'] = 'Er: %s: %s: %s';
$string['replyto'] = 'Erantzun honi:';
$string['stickydescription'] = 'Nabarmendutako gaiak orri guztien goiko aldean daude';
$string['stickytopics'] = 'Nabarmendutako gaiak';
$string['strftimerecentfullrelative'] = '%%v, %%l:%%M %%p';
$string['strftimerecentrelative'] = '%%v, %%k:%%M';
$string['subscribetoforum'] = 'Harpidetu foroan';
$string['subscribetotopic'] = 'Harpidetu gaian';
$string['timeleftnotice'] = '%s minutu geratzen zaizu edizioa bukatzeko';
$string['today'] = 'Gaur';
$string['topicclosedsuccess'] = 'Gaiak ondo itxi dira';
$string['topicisclosed'] = 'Gai hori itxita dago. Soilik moderatzaileek eta taldearen administratzaileek bidal ditzazkete erantzun berriak';
$string['topiclower'] = 'gai';
$string['topicopenedsuccess'] = 'Gaiak ondo ireki dira';
$string['topicslower'] = 'gai';
$string['topicstickysuccess'] = 'Gaiak ondo gainjarri dira';
$string['topicsubscribesuccess'] = 'Gaiak ondo harpidetu dira';
$string['topicsuccessfulunsubscribe'] = 'Gaiaren harpidetza ondo eten da';
$string['topicunstickysuccess'] = 'Gaia ondo kendu da gainjarritakoen artetik';
$string['topicunsubscribesuccess'] = 'Gaien harpidetza ondo eten da';
$string['topicupdatefailed'] = 'Ezini izan dira gaiak eguneratu';
$string['typenewpost'] = 'Foroko mezu berria';
$string['unsubscribefromforum'] = 'Eten fororako harpidetza';
$string['unsubscribefromtopic'] = 'Eten gairako harpidetza';
$string['updateselectedtopics'] = 'Eguneratu aukeratutako gaiak';
$string['whocancreatetopics'] = 'Nork sor ditzakeen gaiak';
$string['yesterday'] = 'Atzo';
$string['youarenotsubscribedtothisforum'] = 'Ez zaude foro honetara harpidetuta';
$string['youarenotsubscribedtothistopic'] = 'Ez zaude gai honetara harpidetuta';
$string['youcannotunsubscribeotherusers'] = 'Ezin duzu beste erabiltzaileen harpidetza eten';
?>
