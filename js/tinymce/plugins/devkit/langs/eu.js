// UK lang variables

tinyMCE.addToLang('devkit',{
title : 'TinyMCE Development Kit',
info_tab : 'Info',
settings_tab : 'Ezarpenak',
log_tab : 'Erregistroa',
content_tab : 'Edukia',
command_states_tab : 'Komandoak',
undo_redo_tab : 'Desegin/Berregin',
misc_tab : 'Denetarik',
filter : 'Filtroa:',
clear_log : 'Erregistroa garbitu',
refresh : 'Erreberritu',
info_help : 'Erreberritu sakatu informazioa ikusteko.',
settings_help : 'Erreberritu sakatu TinyMCE_Control instantzi bakoitzaren ezarpenak erakusteko.',
content_help : 'Erreberritu sakatu ilera eta garbituriko HTML edukia TinyMCE_Control instantzia erakusteko.',
command_states_help : 'Erreberritu sakatu oraingo komandoen egoera ikusteko inst.queryCommandState-tik. Zerrenda honek soporte gabeko komandoak ere markatuko ditu.',
undo_redo_help : 'Erreberritu sakatu desegin/berregin mailak eta instantzia orokorra erakusteko.',
misc_help : 'Hemen zenbait tresna daude debugging eta garapenerako.',
debug_events : 'Debug ekitaldiak',
undo_diff : 'Diff undo levels'
});
