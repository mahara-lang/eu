// UK lang variables

tinyMCE.addToLang('advirudia',{
tab_general : 'Orokorra',
tab_appearance : 'Itxura',
tab_advanced : 'Aurreratua',
general : 'Orokorra',
title : 'Izenburua',
preview : 'Aurre-bista',
constrain_proportions : 'Proportzioak mantendu',
langdir : 'Hizkuntzaren helbidea',
langcode : 'Hizkuntzaren kodea',
long_desc : 'Deskribapen luzeari lotura',
style : 'Estiloa',
classes : 'Motak',
ltr : 'Ezkerretik eskuinera',
rtl : 'Eskuinetik ezkerrera',
id : 'Id',
image_map : 'Irudi mapa',
swap_image : 'Swap irudia',
alt_image : 'Irudi alternatiboa',
mouseover : 'xagua gainetik pasatzeko',
mouseout : 'xagua kanpo geratzean',
misc : 'Denetarik',
example_img : 'Itxura&nbsp;aurre-bista&nbsp;irudia',
missing_alt : 'Ziur al zaude Irudiaren Deskribapena egin gabe aurrera egin nahi duzula? Hau gabe urritasunak dituzten zenbait erabiltzailek ezingo dute irudia ulertu, edo testu nabigatzailea edo irudiak ikusteko ezgaituta nabigatzaileetan ere ez.'
});
