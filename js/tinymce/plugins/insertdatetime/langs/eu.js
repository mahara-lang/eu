// UK lang variables

tinyMCE.addToLang('',{
insertdate_def_fmt : '%Y-%m-%d',
inserttime_def_fmt : '%H:%M:%S',
insertdate_desc : 'Data txertatu',
inserttime_desc : 'Ordua txertatu',
inserttime_months_long : new Array("Urtarrila", "Otsaila", "Martxoa", "Apirila", "Maiatza", "Ekaina", "Uztaila", "Abuztua", "Iraila", "Urria", "Azaroa", "Abendua"),
inserttime_months_short : new Array("Urt", "Ots", "Mart", "Apir", "Mai", "Ek", "Uzt", "Abuz", "Ira", "Urr", "Az", "Ab"),
inserttime_day_long : new Array("Igandea", "Astelehena", "Asteartea", "Asteazkena", "Osteguna", "Ostirala", "Larunbata", "Igandea"),
inserttime_day_short : new Array("Ig", "Astl", "Astr", "Astz", "Ostg", "Ostr", "Lar", "Ig")
});
