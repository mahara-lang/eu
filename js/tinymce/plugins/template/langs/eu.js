// UK lang variables

tinyMCE.addToLang('eskema',{
title : 'Eskemak',
label : 'Eskema',
desc_label : 'Deskribapena',
desc : 'Aurrezehaztutako eskema-edukia txertatu',
select : 'Eskema bat aukeratu',
preview : 'Aurre-ikuspena',
warning : 'Adi: Eskema bat beste batetan eguneratzeak datu galera ekar dezake.',
def_date_format : '%Y-%m-%d %H:%M:%S',
months_long : new Array("Urtarrila", "Otsaila", "Martxoa", "Apirila", "Maiatza", "Ekaina", "Uztaila", "Abuztua", "Iraila", "Urria", "Azaroa", "Abendua"),
months_short : new Array("Urt", "Ots", "Mar", "Ap", "Mai", "Ek", "Uzt", "Ab", "Ir", "Urr", "Az", "Ab"),
day_long : new Array("Igandea", "Astelehena", "Asteartea", "Asteazkena", "Osteguna", "Ostirala", "Larunbata", "Igandea"),
day_short : new Array("Ig", "Astl", "Astr", "Astz", "Ostg", "Ostr", "Lar", "Ig")
});
