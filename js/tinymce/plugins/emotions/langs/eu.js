// UK lang variables

tinyMCE.addToLang('emotikonoak',{
title : 'Emotikonoa txertatu',
desc : 'Emotikonoak',
cool : 'Ondo',
cry : 'Negarrez',
embarassed : 'Lotsatuta',
foot_in_mouth : 'Janaria ahoan',
frown : 'Haserre',
innocent : 'Inuxente',
kiss : 'Musua',
laughing : 'Parrez',
money_mouth : 'Dirua ahoan',
sealed : 'Hermetikoki itxia',
smile : 'Irrifarrea',
surprised : 'Harrituta',
tongue_out : 'Mihia atera',
undecided : 'Duda-mudatan',
wink : 'Begi-keinua',
yell : 'Oihua'
});
