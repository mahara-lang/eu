// UK lang variables

tinyMCE.addToLang('',{
searchreplace_search_desc : 'Bilatu',
searchreplace_searchnext_desc : 'Berriro bilatu',
searchreplace_replace_desc : 'Bilatu/Ordezkatu',
searchreplace_notfound : 'Bilaketa burutu da. Bilatutako katea ez da aurkitua izan.',
searchreplace_search_title : 'Bilatu',
searchreplace_replace_title : 'Bilatu/Ordezkatu',
searchreplace_allreplaced : 'Bilatutako katea azaltzen den leku guztietan ordezkatu da.',
searchreplace_findwhat : 'Zer bilatu',
searchreplace_replacewith : 'Honekin ordezkatu',
searchreplace_direction : 'Helbidea',
searchreplace_up : 'Gora',
searchreplace_down : 'Behera',
searchreplace_case : 'Match case',
searchreplace_findnext : 'Bilatu&nbsp;hurrengoa',
searchreplace_replace : 'Ordezkatu',
searchreplace_replaceall : 'Ordezkatu&nbsp;guztiak',
searchreplace_cancel : 'Ezeztatu'
});
