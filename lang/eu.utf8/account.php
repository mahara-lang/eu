<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['accountdeleted'] = 'Zure kontua ezabatu egin da.';
$string['accountoptionsdesc'] = 'Kontuaren aukera orokorrak';
$string['changepassworddesc'] = 'Pasahitz berria';
$string['changepasswordotherinterface'] = '<a href="%s">Pasahitza aldatu</a> dezakezu beste interfaze batean barrena</a>';
$string['changeusername'] = 'Erabiltzaile berria';
$string['changeusernamedesc'] = '%s(e)ra sartzeko erabiltzen duzun erabiltzaile-izena. Erabiltzaile-izena 3 eta 30 karaktere bitartekoa izan daiteke, eta hizkiak, zenbakiak eta ikur erabilienak izan ditzake, tarte hutsak izan ezik.';
$string['changeusernameheading'] = 'Aldatu erabiltzaile-izena';
$string['deleteaccount'] = 'Ezabatu kontua';
$string['deleteaccountdescription'] = 'Zure kontua ezabatzen baduzu, beste erabiltzaieek ezin izango dituzte zure profileko informazioa eta zure Ikuspegiak ikusi. Zuk foroetara bidalitako mezuak ikusi egingo dira baina egilearen izena ez da agertuko.';
$string['disabled'] = 'Desgaituta';
$string['disablemultipleblogserror'] = 'Ezin duzu hainbat egunkari desgaitu bakarra baituzu.';
$string['enabled'] = 'Gaituta';
$string['enablemultipleblogs'] = 'Gaitu hainbat egunkari';
$string['enablemultipleblogsdescription'] = 'Berez, egunkari bakarra duzu. Bat baino gehiago izan nahi baduzu, markatu aukera hau.';
$string['friendsauth'] = 'Lagun berriek nire onarpena behar dute.';
$string['friendsauto'] = 'Lagun berriak automatikoki daude onartuta.';
$string['friendsdescr'] = 'Lagun-kontrola';
$string['friendsnobody'] = 'Inork ezin nazake lagun gisa gehitu.';
$string['hiderealname'] = 'Ezkutatu benetako izena';
$string['hiderealnamedescription'] = 'Markatu hau erakusteko izena ezarri baduzu eta ez baduzu nahi beste erabiltzaileek zu aurkitzeko aukera izatea erabiltzaileak benetako izenaz bilatzean';
$string['language'] = 'Hizkuntza';
$string['maildisabled'] = 'E-posta desgaituta';
$string['maildisabledbounce'] = 'Zure e-postara mezuak bidaltzea desgaitu egin da mezu gehiegi itzuli baitira zerbitzarira. Mesedez, ziurtatu zure e-posta kontua behar bezala dabilela berriz ere zure e-posta kontuko %s-n hobespenetan gaitu aurretik';
$string['maildisableddescription'] = 'Zure e-postara mezuak bidaltzea desgaitu egin da. <a href="%s">zure e-posta berriz gai dezakezu</a>kontuaren hobespen orritik.';
$string['messagesallow'] = 'Utzi edonori mezuak bidaltzen niri.';
$string['messagesdescr'] = 'Beste erabiltzaileengandik jasotako mezuak';
$string['messagesfriends'] = 'Utzi \'Nire Lagunak\' zerrendako kideei niri mezuak bidaltzen.';
$string['messagesnobody'] = 'Ez utzi inori niri mezuak bidaltzen.';
$string['mobileuploadtoken'] = 'Mobiletik igotzeko token-a';
$string['mobileuploadtokendescription'] = 'Sartu hemen eta zure telefonoan token-a igotzea gaitzeko (oharra: aldatu egingo da igotzen duzun bakoitzean. <br/>Arazorik baduzu, besterik gabe berrabiarazi berau hemen eta zure telefonoan.';
$string['off'] = 'Desaktibatuta';
$string['oldpasswordincorrect'] = 'Hau ez da zure oraingo pasahitza.';
$string['on'] = 'Aktibatuta';
$string['prefsnotsaved'] = 'Zure hobespenak gordetzerakoan akats bat egon da!';
$string['prefssaved'] = 'Hobespenak gorde egin dira.';
$string['showhomeinfo'] = 'Erakutsi Mahara-ri buruzko informazioa hasiera-orrian';
$string['showviewcolumns'] = 'Erakutsi zutabeak gehitu eta kentzeko kontrolak ikuspegia editatzean.';
$string['tagssideblockmaxtags'] = 'Gehienezko etiketak lainoan';
$string['tagssideblockmaxtagsdescription'] = 'Erakutsi beharreko gehienezko etiketa-kopurua zure etiketa-hodeian';
$string['updatedfriendcontrolsetting'] = 'Lagunen kontrola eguneratuta';
$string['usernameexists'] = 'Erabiltzaile-izen hau dagoeneko erabilita dago; mesedez, aukeratu beste bat.';
$string['wysiwygdescr'] = 'HTML editorea';
?>
