<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['accessdenied'] = 'Sarbidea ukatuta';
$string['accessdeniedexception'] = 'Ez duzu orrialde hau ikusteko baimenik';
$string['apcstatoff'] = 'Badirudi zure zerbitzariak APC-a apc.stat=0 ezarrita duela. Mahara-k ez du onartzen konfigurazio hori. apc.stat=1 ezarri behar duzu php.ini fitxategian. Ostatua sarean baduzu, ziurrenik ezin izango duzu gauza handirik egin apc.stat aktibatzeko zure hornitzaileari eskatzen ez badiozu. Agian, pentsatu egin beharko zenuke beste ostatu bat hartzea.';
$string['artefactnotfound'] = 'Ez da aurkitu %s id-dun gailurik';
$string['artefactnotfoundmaybedeleted'] = 'Ez da aurkitu %s id-dun gailurik(dagoeneko ezabatu egingo ote zuten?)';
$string['artefactnotinview'] = '%s gailua ez dago %s orrian';
$string['artefactonlyviewableinview'] = 'Mota honetako tresnak orri batean bakarrik ikus daitezke';
$string['artefactpluginmethodmissing'] = '%s plugindun gailuak %s inplementatu behar du eta ez du egiten.';
$string['artefacttypeclassmissing'] = 'Gailu-mota guztiek klase bat inplementatu behar dute. %s galduta';
$string['artefacttypemismatch'] = 'Gailu-mota nahastuta; %s erabiltzen saiatzen ari zara %s bezala.';
$string['artefacttypenametaken'] = '%s motako gailua beste plugin batek (%s) hartu du dagoeneko.';
$string['blockconfigdatacalledfromset'] = 'Configdata ez litzateke zuzenean ezarri behar; erabili PluginBlocktype::instance_config_save instead';
$string['blockinstancednotfound'] = 'Ez da aurkitu %s id-dun bloke-instantziarik';
$string['blocktypelibmissing'] = 'lib.php galduta %s blokerako %s plugindun gailuan';
$string['blocktypemissingconfigform'] = '%s motako blokeak instance_config_form inplementatu behar du.';
$string['blocktypenametaken'] = '%s motako blokea dagoeneko beste plugin batek hartu du (%s)';
$string['blocktypeprovidedbyartefactnotinstallable'] = 'Hau %s plugindun gailuaren instalazioaren parte bezala instalatuko da.';
$string['classmissing'] = '%splugineko %s motarako %s klasea galdu egin da';
$string['couldnotmakedatadirectories'] = 'Arrazoiren batengatik, datu-nukleoaren direktorio batzuk ezin izan dira sortu. Horrek ez luke gertatu behar, Maharak aurrez detektatu balu datu-erroa berridazgarria dela. Mesedez, berrikusi datu-erro direktorioaren baimenak.';
$string['curllibrarynotinstalled'] = 'Zure zerbitzariaren konfigurazioak ez du jasotzen curl-luzapena. Maharak horren beharra du Moodlen txertatzeko eta kanpoko feed-a berreskuratzeko. Mesedez, egiaztatu curl kargatuta dagoela php.ini-n, edo beharrezkoa bada, instalatu.';
$string['datarootinsidedocroot'] = 'Zure datu-erroa dokumentu-erroaren barruan jarri behar duzu. Hau aspaldiko segurtasun arazoa da, inork ezin du eskatu zuzenean saioaren daturik (beste pertsona batzuen saioa bahitu ordez), edo beste batzuek igotako fitxategirik, horietara sartzeko baimenik izan ezean. Mesedez, konfiguratu datu-erroa dokumentu-errotik kanpo egoteko.';
$string['datarootnotwritable'] = 'Definituta duzu datu-erro direktorioa, %s, ezin dela berridatzi. Ondorioz, zure zerbitzarian ezin da gorde ez saioen daturik, ez erabiltzaileen fitxategirik, ez igotzea behar duen ezer. Mesedez, osatu direktorioa oraindik ez bada existitzen, eta bestela, eman direktorioaren jabetza web-zerbitzariaren erabiltzaileari.';
$string['dbconnfailed'] = 'Maharak ezin du aplikazioaren datu-basearekin konektatu.

 * Mahara erabiltzen ari bazara, mesedez itxaron minutu bat eta saiatu berriro * Administratzailea baldin bazara, mesedez berrikusi zure datu-basearen ezarpenak eta egiaztatu zure datu-baseak ez duela akatsik.

Jasotako errorea ondokoa da:';
$string['dbnotutf8'] = 'Ez zara ari UTF-8 datu-basea erabiltzen. Maharak datu guztiak biltzen ditu UTF-8 bezala. Mesedez, baztertu zure datu-basea eta sor ezazu berriro UTF-8 kodea erabilita.';
$string['dbversioncheckfailed'] = 'Zure datu-base zerbitzariaren bertsioa ez da nahiko berria Mahararekin lan egiteko. Zure zerbitzaria %s %s da, baina Maharak %s bertsioa behar du gutxienez.';
$string['domextensionnotloaded'] = 'Zure zerbitzariaren ezarpenetan ez dago dom luzapena. Mahara-k beharrezkoa du hau hainbat iturritako XML datuak aztertzeko.';
$string['gdextensionnotloaded'] = 'Zure zerbitzariaren konfigurazioak ez du gd luzapenik. Maharak hori behar du kargaturiko irudien neurria aldatu zein beste eragiketa batzuek egiteko. Mesedez, ziurtatu php.ini-n kargatua dagoela, edo beharrezkoa bada, instalatu.';
$string['gdfreetypenotloaded'] = 'Zure zerbitzariak duen gd-luzapenaren konfigurazioak ez du jasotzen Freetype-euskarria. Maharak beharrezkoa du CAPTCHA irudiak sortzeko. Mesedez, egiaztatu gd Freetype euskarriarekin dagoela konfiguratuta.';
$string['interactioninstancenotfound'] = 'Ez da aurkitu %s id-dun jarduera-instantziarik.';
$string['invaliddirection'] = 'Baliogabeko helbidea: %s';
$string['invalidviewaction'] = 'Orriaren kontrolerako ekintza baliogabea: %s';
$string['jsonextensionnotloaded'] = 'Zure zerbitzariaren konfigurazioak ez du JSON luzapenik. Maharak hura behar du nabigatzailera datuak bidali eta jasotzeko. Mesedez, ziurtatu php.ini-n kargatua dagoela, edo beharrezkoa bada, instalatu.';
$string['magicquotesgpc'] = 'PHP-ezarpen arriskutsuak dituzu, magic_quotes_gpc aktibatuta dago. Mahara lanean ari da horretan, baina konpontzen saiatu beharko zenuke';
$string['magicquotesruntime'] = 'PHP-ezarpen arriskutsuak dituzu, magic_quotes_runtime aktibatuta dago. Mahara lanean ari da horretan, baina konpontzen saiatu beharko zenuke.';
$string['magicquotessybase'] = 'PHP-ezarpen arriskutsuak dituzu, magic_quotes_sybase aktibatuta dago. Mahara lanean ari da horretan, baina konpontzen saiatu beharko zenuke.';
$string['missingparamblocktype'] = 'Saia zaitez lehenik gehitzeko bloke-mota bat aukeratzen.';
$string['missingparamcolumn'] = 'Zutabeari buruzko zehaztapena galduta';
$string['missingparamid'] = 'Id galduta';
$string['missingparamorder'] = 'Aginduari buruzko zehaztapena galduta';
$string['mysqldbextensionnotloaded'] = 'Zure zerbitzariaren konfigurazioak ez du mysql luzapena jasotzen. Maharak beharrezko du datuak biltzeko erlaziozko datu-base batean. Mesedez, egiaztatu php.ini-n kargatuta dagoela, edo beharrezkoa bada, instalatu.';
$string['notartefactowner'] = 'Ez zara gailu honen jabea';
$string['notfound'] = 'Ez da aurkitu';
$string['notfoundexception'] = 'Ez da aurkitu bilatzen ari zaren orria';
$string['onlyoneblocktypeperview'] = 'Ezin da orri batean %s bloke-motako bat baino gehiago izarri.';
$string['onlyoneprofileviewallowed'] = 'Profil-orri bakarrerako baimena duzu.';
$string['parameterexception'] = 'Beharrezkoa den parametro bat galdu egin da';
$string['pgsqldbextensionnotloaded'] = 'Zure zerbitzariaren konfigurazioak ez du pgsql luzapena jasotzen. Maharak beharrezko du datuak biltzeko erlaziozko datu-base batean. Mesedez, egiaztatu php.ini-n kargatuta dagoela, edo beharrezkoa bada, instalatu.';
$string['phpversion'] = 'Mahara ez dabil PHP < %s. Mesedez eguneratu zure PHP bertsioa, edo mugitu Mahara beste host batetara.';
$string['postmaxlessthanuploadmax'] = 'Zure PHPren post_max_size ezarpena (%s)  upload_max_filesize ezarpena (%s) baino txikiagoa da.  %s baino handiagoak diren igotako fitxategiek huts egingo dute, errore mezua emanez. Normalean, post_max_size -k upload_max_filesize baino askoz handiagoa behar luke izan.';
$string['registerglobals'] = 'PHP ezarpen arriskutsuak dituzu, register_globals aktibatuta dago. Mahara lanean ari da horretan, baina konpontzen saiatu beharko zenuke';
$string['safemodeon'] = 'Zure zerbitzaria modu seguruan dabilela dirudi. Mahara ez da bateragarria modu seguruarekin. Desaktibatu egin beharko zenuke php.ini fitxategian, edo gunearen zure apache config-en.
Partekatutako host batean baldin bazaude, beharbada, zure host kudeatzaileari safe_mode desaktibatzeko eskatzea da egin dezakezun bakarra. Agian, beste host batetara mugitzea pentsatu beharko zenuke.';
$string['sessionextensionnotloaded'] = 'Zure zerbitzariaren konfigurazioak ez du saioaren luzapenik. Maharak beharrezko du erabiltzaileen sarrera bermatzeko. Mesedez ziurtatu php.ini-n kargatua dagoela, edo beharrezkoa bada, instalatu.';
$string['smallpostmaxsize'] = 'Zure PHPren post_max_size ezarpena (%s) txikiegia da.  %s baino handiagoak diren fitxategiak igotzeak huts egingo du, errore mezua emanez.';
$string['themenameinvalid'] = '\'%s\' itxuraren izenak onartzen ez diren karaktereak dauzka.';
$string['timezoneidentifierunusable'] = 'Zure webgunearen ostalariko PHPak ez du itzultzen leku-eremuaren identifikatzailearen balore onargarririk (%%z) - datu-formato batzuk, hala nola LEAP2A export, hautsiko dira. %%z PHP data-formatuko kodea da. Arazoa honen zergatia normalean PHP Windowspean korritzean datza.';
$string['unknowndbtype'] = 'Zure zerbitzariaren konfigurazioak datu-base mota ezezaguna aipatzen du. Onartutako balioak "postgres8" eta "mysql5" dira. Mesedez, aldatu datu-base motaren ezarpenak config.php-n';
$string['unrecoverableerror'] = 'Berreskuraezina den errore bat gertatu da. Seguruenik, horrek esan nahi du akats edo bug bat aurkitu duzula sisteman.';
$string['unrecoverableerrortitle'] = '%s - Gune Erabilgaitza';
$string['versionphpmissing'] = '%s %s plugina galduta dago php bertsioan';
$string['viewnotfound'] = '%s id-dun orria ez da aurkitu';
$string['viewnotfoundexceptionmessage'] = 'Existitzen ez den orri batetan sartu nahiean zabiltza!';
$string['viewnotfoundexceptiontitle'] = 'Ez da orria aurkitu';
$string['xmlextensionnotloaded'] = 'Zure zerbitzariaren konfigurazioak ez du %s luzapena onartzen. Maharak beharrezko du hainbat iturritatik jasotako XML datuak bihurtzeko. Mesedez ziurtatu php.ini-n kargatua dagoela, edo beharrezkoa bada, instalatu.';
$string['youcannotviewthisusersprofile'] = 'Ezin duzu erabiltzaile honen profila ikusi';
?>
