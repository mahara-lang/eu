<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['All'] = 'Guztia';
$string['Artefact'] = 'Tresna';
$string['Artefacts'] = 'Tresnak';
$string['Close'] = 'Itxi';
$string['Content'] = 'Edukia';
$string['Copyof'] = '%s(r)en kopia';
$string['Created'] = 'Sortutakoak';
$string['Failed'] = 'Kale egin du';
$string['From'] = 'Noiztik';
$string['Help'] = 'Laguntza';
$string['Hide'] = 'Ezkutatu';
$string['Invitations'] = 'Gonbidapenak';
$string['Memberships'] = 'Partaidetzak';
$string['Organise'] = 'Antolatu';
$string['Permissions'] = 'Baimenak';
$string['Query'] = 'Kontsulta';
$string['Requests'] = 'Eskariak';
$string['Results'] = 'Emaitzak';
$string['Site'] = 'Gunea';
$string['Tag'] = 'Etiketa';
$string['To'] = 'Noiz arte';
$string['Total'] = 'Denera';
$string['Updated'] = 'Eguneratuta:';
$string['Visits'] = 'Ikustaldiak';
$string['about'] = 'Honi buruz';
$string['accept'] = 'Onartu';
$string['accessforbiddentoadminsection'] = 'Debekatua duzu kudeaketa-eremura sartzea';
$string['accesstotallydenied_institutionsuspended'] = 'Zure %s erakundea indargabetu egin da. Berriz ere aktibatu arte, ezin izango zara %s-n identifikatu. Mesedez, jarri harremanetan zure erakundearekin laguntza lortzeko.';
$string['account'] = 'Nire kontua';
$string['accountcreated'] = '%s: Kontu berria';
$string['accountcreatedchangepasswordhtml'] = '<p>Kaixo %s</p>

<p><a href="%s">%s(e)n kontu berri bat sortu da zuretzat.</a> Xehetasunak ondokoak dira:</p>

<ul>
    <li><strong>Erabiltzaile-izena:</strong> %s</li>
    <li><strong>Pasahitza:</strong> %s</li>
</ul>

<p>Lehen aldiz sartzean pasahitza aldatzeko eskatuko dizu sistemak.</p>

<p>Hasteko bisitatu <a href="%s">%s</a>!</p>

<p>Ondo izan, %s Guneko kudeatzailea</p>';
$string['accountcreatedchangepasswordtext'] = 'Kaixo %s,

%s(e)n kontu berri bat sortu da zuretzat. Xehetasunak ondokoak dira:

Erabiltzaile-izena: %s
Pasahitza: %s

Lehen aldiz sartzean pasahitza aldatzeko eskatuko dizu sistemak.

Hasteko bisitatu %s!

Ondo izan, %s Guneko kudeatzailea';
$string['accountcreatedhtml'] = '<p>kaixo %s</p>

<p><a href="%s">%s(e)n kontu berri bat sortu da zuretzat. </a>Xehetasunak ondokoak dira:</p>

<ul>
    <li><strong>Erabiltzaile-izena:</strong> %s</li>
    <li><strong>Pasahitza:</strong> %s</li>
</ul>

<p>Hasteko bisitatu <a href="%s">%s</a>!</p>

<p>Ondo izan, %s Guneko kudeatzailea</p>';
$string['accountcreatedtext'] = 'Kaixo %s,

%s(e)n kontu berri bat sortu da zuretzat. Xehetasunak ondokoak dira:

Erabiltzaile-izena: %s
Pasahitza: %s

Hasteko bisitatu %s!

Ondo izan, %s Guneko kudeatzailea';
$string['accountdeleted'] = 'Sentitzen dugu, zure kontua ezabatu egin da';
$string['accountexpired'] = 'Sentitzen dugu, zure kontua iraungita dago';
$string['accountexpirywarning'] = 'Kontuaren iraungitze-oharra';
$string['accountexpirywarninghtml'] = '&lt;p&gt;Kaixo %s,&lt;/p&gt;
    
&lt;p&gt;%s(e)n duzun kontua iraungi egingo da %s barru.&lt;/p&gt;

&lt;p&gt;Portfolioan duzun edukia gordetzea gomendatzen dizugu; horretarako, Esportatu erabil dezakezu. Baliabide hori erabiltzeko argibideak erabiltzailearen gidan aurki ditzakezu.&lt;/p&gt;

&lt;p&gt;Kontua erabiltzeko epea luzatu nahi baduzu edo galderaren bat baduzu, &lt;a href=&quot;%s&quot;&gt;jarri harremanetan gurekin&lt;/a&gt;, mesedez.&lt;/p&gt;

&lt;p&gt;Ondo izan, %s Guneko kudeatzailea&lt;/p&gt;';
$string['accountexpirywarningtext'] = 'Kaixo %s,

%s(e)n duzun kontua iraungi egingo da %s barru.

Portfolioan duzun edukia gordetzea gomendatzen dizugu; horretarako, Esportatu erabil dezakezu. Baliabide hori erabiltzeko argibideak erabiltzailearen gidan aurki ditzakezu.

Kontua erabiltzeko epea luzatu nahi baduzu edo galderaren bat baduzu, jarri harremanetan gurekin, mesedez:

%s

Ondo izan, %s Guneko kudeatzailea';
$string['accountinactive'] = 'Sentitzen dugu, zure kontua ez dago aktibo orain';
$string['accountinactivewarning'] = 'Kontuaren jarduerarik ezari buruzko oharra';
$string['accountinactivewarninghtml'] = '&lt;p&gt;Kaixo %s,&lt;/p&gt;

&lt;p&gt;%s(e)n duzun kontua ez-aktibo bihurtuko da %s barru.&lt;/p&gt;

&lt;p&gt;Hori gertatutakoan, ezin izango zara gunean sartu kudeatzaile batek zure kontua berrezarri bitartean.&lt;/p&gt;

&lt;p&gt;Gunean sartuta kontua desaktibatzea ekidin dezakezu.&lt;/p&gt;

&lt;p&gt;Ondo izan, %s Guneko kudeatzailea&lt;/p&gt;';
$string['accountinactivewarningtext'] = 'Kaixo %s,

%s(e)n duzun kontua ez-aktibo bihurtuko da %s barru.

Hori gertatutakoan, ezin izango zara gunean sartu kudeatzaile batek zure kontua berrezarri bitartean.

Gunean sartuta kontua desaktibatzea ekidin dezakezu.

Ondo izan, %s Gunearen kudeatzailea';
$string['accountprefs'] = 'Hobespenak';
$string['accountsuspended'] = 'Zure kontua eten egin da %s gisa. Blokeoaren arrazoia ondokoa da: <blockquote>%s</blockquote>';
$string['activityprefs'] = 'Jarduera-hobespenak';
$string['add'] = 'Gehitu';
$string['addemail'] = 'Gehitu e-posta helbidea';
$string['adminphpuploaderror'] = 'Errorea gertatu da fitxategia igotzean, ziurrenik zure zerbitzariaren konfigurazioagatik.';
$string['after'] = 'ondoren';
$string['allowpublicaccess'] = 'Onartu sarbide publikoa (kontura sartu ez direnena)';
$string['alltags'] = 'Etiketa guztiak';
$string['allusers'] = 'Erabiltzaile guztiak';
$string['alphabet'] = 'A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z';
$string['applychanges'] = 'Aplikatu aldaketak';
$string['artefact'] = 'tresna';
$string['artefactnotfound'] = 'Ez da aurkitu %s id-dun tresnarik';
$string['artefactnotpublishable'] = '%s tresna ezin da argitaratu %s orrian';
$string['artefactnotrendered'] = 'Ez da tresna prozesatu';
$string['at'] = 'hona';
$string['attachment'] = 'Eranskina';
$string['back'] = 'Atzera';
$string['backto'] = 'Itzuli %s(r)a';
$string['before'] = 'aurretik';
$string['belongingto'] = 'Jabetza';
$string['blacklisteddomaininurl'] = 'Eremu honetako url batek zerrenda beltzeko domeinua dauka: %s.';
$string['bytes'] = 'byteak';
$string['cancel'] = 'Utzi';
$string['cancelrequest'] = 'Eskaera ezeztatu';
$string['cannotremovedefaultemail'] = 'Ezin duzu lehenetsitako e-posta ezabatu';
$string['cantchangepassword'] = 'Sentitzen dugu, ezin duzu pasahitza aldatu interfaze honekin - mesedez erabili zure erakundearena';
$string['captchadescription'] = 'Jarri eskuinaldeko irudian ikusten dituzun hizkiak. Larriak eta xeheak ez dira bereizten.';
$string['captchaimage'] = 'CAPTCHA Irudia';
$string['captchaincorrect'] = 'Jarri hizkiak irudian ikusten diren bezala';
$string['captchatitle'] = 'CAPTCHA Irudia';
$string['change'] = 'Aldatu';
$string['changepassword'] = 'Aldatu pasahitza';
$string['changepasswordinfo'] = 'Erabili aurretik, pasahitza aldatzeko eskatuko dizu sistemak.';
$string['choosetheme'] = 'Aukera ezazu itxura...';
$string['chooseusernamepassword'] = 'Aukera itzazu erabiltzale-izena eta pasahitza';
$string['chooseusernamepasswordinfo'] = 'Erabiltzaile-izena eta pasahitza behar dituzu %s(r)a sartzeko. Mesedez, aukeratu orain.';
$string['clambroken'] = 'Zure kudeatzailea gaituta dago igotzen diren fitxategiek birusik duten egiaztatzeko, baina zerbait gaizki konfiguratuta dauka. Zure fitxategia EZ da ondo igo. Mezu elektronikoa bidali zaio zure kudeatzaileari konponbidea bila dezan. Saiatu geroago igotzen.';
$string['clamdeletedfile'] = 'Fitxategia ezabatu da';
$string['clamdeletedfilefailed'] = 'Fitxategia ezin da ezabatu';
$string['clamemailsubject'] = '%s :: Clam AV jakinarazpena';
$string['clamfailed'] = 'Clam AV-ren funtzionamenduak huts egin du. %s da bidalitako mezua. Hau da clam-aren output-a:';
$string['clamlost'] = 'Clam AV fitxategiak igotzean funtzionatzeko dago konfiguratuta, baina Clam AV-ri emandako bidea, %s, okerra da.';
$string['clammovedfile'] = 'Fitxategia berrogeialdi-direktoriora mugitu da.';
$string['clamunknownerror'] = 'Akats ezezaguna gertatu da clam-arekin errore ezezagun bat gertatu da.';
$string['collapse'] = 'Txikitu';
$string['complete'] = 'Osatua';
$string['config'] = 'Konfig';
$string['confirmdeletetag'] = 'Ziur al zaude etiketa hau zure portfolio osoan ezabatu nahi duzula?';
$string['confirminvitation'] = 'Gonbidapena baieztatu';
$string['confirmpassword'] = 'Pasahitza baieztatu';
$string['contactus'] = 'Jarri harremanetan gurekin';
$string['cookiesnotenabled'] = 'Zure nabigatzaileak ez ditu cookiak gaituta, edo guneak blokeatu egiten ditu. Maharak cookiak behar ditu sartzen uzteko';
$string['couldnotgethelp'] = 'Errorea gertatu da laguntza-orria berreskuratzen saiatzean';
$string['country.ad'] = 'Andorra';
$string['country.ae'] = 'Arabiar Emirato Batuak';
$string['country.af'] = 'Afganistan';
$string['country.ag'] = 'Antigua eta Barbuda';
$string['country.ai'] = 'Anguilla';
$string['country.al'] = 'Albania';
$string['country.am'] = 'Armenia';
$string['country.an'] = 'Antilla holandarrak';
$string['country.ao'] = 'Angola';
$string['country.aq'] = 'Antartika';
$string['country.ar'] = 'Argentina';
$string['country.as'] = 'Amerikako Samoa';
$string['country.at'] = 'Austria';
$string['country.au'] = 'Australia';
$string['country.aw'] = 'Aruba';
$string['country.ax'] = 'Aland Uharteak';
$string['country.az'] = 'Azerbaijan';
$string['country.ba'] = 'Bosnia-Herzegovina';
$string['country.bb'] = 'Barbados';
$string['country.bd'] = 'Bangladesh';
$string['country.be'] = 'Belgika';
$string['country.bf'] = 'Burkina Faso';
$string['country.bg'] = 'Bulgaria';
$string['country.bh'] = 'Bahrain';
$string['country.bi'] = 'Burundi';
$string['country.bj'] = 'Benin';
$string['country.bm'] = 'Bermudak';
$string['country.bn'] = 'Brunei Darussalam';
$string['country.bo'] = 'Bolivia';
$string['country.br'] = 'Brazil';
$string['country.bs'] = 'Bahamas';
$string['country.bt'] = 'Bhutan';
$string['country.bv'] = 'Bouvet Uhartea';
$string['country.bw'] = 'Botswana';
$string['country.by'] = 'Belarus';
$string['country.bz'] = 'Belize';
$string['country.ca'] = 'Kanada';
$string['country.cc'] = 'Cocos (Keeling) Uharteak';
$string['country.cd'] = 'Kongoko Errepublika Demokratikoa';
$string['country.cf'] = 'Afrika Erdiko Errepublika';
$string['country.cg'] = 'Kongo';
$string['country.ch'] = 'Suitza';
$string['country.ci'] = 'Cote D\'ivoire';
$string['country.ck'] = 'Cook Uharteak';
$string['country.cl'] = 'Txile';
$string['country.cm'] = 'Kamerun';
$string['country.cn'] = 'Txina';
$string['country.co'] = 'Kolonbia';
$string['country.cr'] = 'Costa Rica';
$string['country.cs'] = 'Serbia eta Montenegro';
$string['country.cu'] = 'Kuba';
$string['country.cv'] = 'Cape Verde';
$string['country.cx'] = 'Christmas Uhartea';
$string['country.cy'] = 'Txipre';
$string['country.cz'] = 'Txekiar Errepublika';
$string['country.de'] = 'Alemania';
$string['country.dj'] = 'Djibouti';
$string['country.dk'] = 'Danimarka';
$string['country.dm'] = 'Dominika';
$string['country.do'] = 'Dominikar Errepublika';
$string['country.dz'] = 'Aljeria';
$string['country.ec'] = 'Ekuador';
$string['country.ee'] = 'Estonia';
$string['country.eg'] = 'Egipto';
$string['country.eh'] = 'Ekialdeko Sahara';
$string['country.er'] = 'Eritrea';
$string['country.es'] = 'Espainia';
$string['country.et'] = 'Etiopia';
$string['country.eu'] = 'Euskal Herria';
$string['country.fi'] = 'Finlandia';
$string['country.fj'] = 'Fiji';
$string['country.fk'] = 'Falkland Uharteak (Malvinas)';
$string['country.fm'] = 'Micronesiako Estatu Federatuak';
$string['country.fo'] = 'Faroe Uharteak';
$string['country.fr'] = 'Frantzia';
$string['country.ga'] = 'Gabon';
$string['country.gb'] = 'Erresuma Batua';
$string['country.gd'] = 'Grenada';
$string['country.ge'] = 'Georgia';
$string['country.gf'] = 'Guiana frantsesa';
$string['country.gg'] = 'Guernsey';
$string['country.gh'] = 'Ghana';
$string['country.gi'] = 'Gibraltar';
$string['country.gl'] = 'Groenlandia';
$string['country.gm'] = 'Gambia';
$string['country.gn'] = 'Ginea';
$string['country.gp'] = 'Guadalupe';
$string['country.gq'] = 'Ginea ekuatoriarra';
$string['country.gr'] = 'Grezia';
$string['country.gs'] = 'Hego Georgia eta Hego Sandwich Uharteak';
$string['country.gt'] = 'Guatemala';
$string['country.gu'] = 'Guam';
$string['country.gw'] = 'Ginea-bissau';
$string['country.gy'] = 'Guyana';
$string['country.hk'] = 'Hong Kong';
$string['country.hm'] = 'Heard Uhartea eta Mcdonald Uharteak';
$string['country.hn'] = 'Honduras';
$string['country.hr'] = 'Kroazia';
$string['country.ht'] = 'Haiti';
$string['country.hu'] = 'Hungaria';
$string['country.id'] = 'Indonesia';
$string['country.ie'] = 'Irlanda';
$string['country.il'] = 'Israel';
$string['country.im'] = 'Man Uhartea';
$string['country.in'] = 'India';
$string['country.io'] = 'British Indian Ocean Territory';
$string['country.iq'] = 'Irak';
$string['country.ir'] = 'Iran, Islamiar Errepublika';
$string['country.is'] = 'Islandia';
$string['country.it'] = 'Italia';
$string['country.je'] = 'Jersey';
$string['country.jm'] = 'Jamaika';
$string['country.jo'] = 'Jordan';
$string['country.jp'] = 'Japonia';
$string['country.ke'] = 'Kenya';
$string['country.kg'] = 'Kirgzistan';
$string['country.kh'] = 'Kanbodia';
$string['country.ki'] = 'Kiribati';
$string['country.km'] = 'Komoreak';
$string['country.kn'] = 'Saint Kitts eta Nevis';
$string['country.kp'] = 'Koreako Herri Errepublika Demokratikoa';
$string['country.kr'] = 'Koreako Errepublika';
$string['country.kw'] = 'Kuwait';
$string['country.ky'] = 'Cayman Uharteak';
$string['country.kz'] = 'Kazakhstan';
$string['country.la'] = 'Laosko Herri Errepublika Demokratikoa';
$string['country.lb'] = 'Libano';
$string['country.lc'] = 'Santa Luzia';
$string['country.li'] = 'Liechtenstein';
$string['country.lk'] = 'Sri Lanka';
$string['country.lr'] = 'Liberia';
$string['country.ls'] = 'Lesotho';
$string['country.lt'] = 'Lituania';
$string['country.lu'] = 'Luxenburgo';
$string['country.lv'] = 'Latvia';
$string['country.ly'] = 'Libyan Arab Jamahiriya';
$string['country.ma'] = 'Maroko';
$string['country.mc'] = 'Monako';
$string['country.md'] = 'Moldaviako Errepublika';
$string['country.mg'] = 'Madagaskar';
$string['country.mh'] = 'Marshall Uharteak';
$string['country.mk'] = 'Macedoniako Jugoslaviar Errepublika Ohia';
$string['country.ml'] = 'Mali';
$string['country.mm'] = 'Myanmar';
$string['country.mn'] = 'Mongolia';
$string['country.mo'] = 'Macau';
$string['country.mp'] = 'Ipar Mariana Uharteak';
$string['country.mq'] = 'Martinique';
$string['country.mr'] = 'Mauritania';
$string['country.ms'] = 'Montserrat';
$string['country.mt'] = 'Malta';
$string['country.mu'] = 'Maurizio';
$string['country.mv'] = 'Maldivak';
$string['country.mw'] = 'Malawi';
$string['country.mx'] = 'Mexiko';
$string['country.my'] = 'Malaysia';
$string['country.mz'] = 'Mozanbike';
$string['country.na'] = 'Namibia';
$string['country.nc'] = 'Kaledonia Berria';
$string['country.ne'] = 'Niger';
$string['country.nf'] = 'Norfolk Uhartea';
$string['country.ng'] = 'Nigeria';
$string['country.ni'] = 'Nikaragua';
$string['country.nl'] = 'Herbehereak';
$string['country.no'] = 'Norway';
$string['country.np'] = 'Nepal';
$string['country.nr'] = 'Nauru';
$string['country.nu'] = 'Niue';
$string['country.nz'] = 'Zelanda Berria';
$string['country.om'] = 'Oman';
$string['country.pa'] = 'Panama';
$string['country.pe'] = 'Peru';
$string['country.pf'] = 'Frantziar Polinesia';
$string['country.pg'] = 'Papua Ginea Berria';
$string['country.ph'] = 'Filipinak';
$string['country.pk'] = 'Pakistan';
$string['country.pl'] = 'Polonia';
$string['country.pm'] = 'Santa Pierre eta Miquelon';
$string['country.pn'] = 'Pitcairn';
$string['country.pr'] = 'Puerto Rico';
$string['country.ps'] = 'Palestinar Lurraldea, Okupatutako';
$string['country.pt'] = 'Portugal';
$string['country.pw'] = 'Palau';
$string['country.py'] = 'Paraguay';
$string['country.qa'] = 'Qatar';
$string['country.re'] = 'Reunion';
$string['country.ro'] = 'Errumania';
$string['country.ru'] = 'Errusiar Federazioa';
$string['country.rw'] = 'Rwanda';
$string['country.sa'] = 'Saudi Arabia';
$string['country.sb'] = 'Solomon Uharteak';
$string['country.sc'] = 'Seychelles';
$string['country.sd'] = 'Sudan';
$string['country.se'] = 'Suedia';
$string['country.sg'] = 'Singapor';
$string['country.sh'] = 'Santa Helena';
$string['country.si'] = 'Eslobenia';
$string['country.sj'] = 'Svalbard eta Jan Mayen';
$string['country.sk'] = 'Eslobakia';
$string['country.sl'] = 'Sierra Leona';
$string['country.sm'] = 'San Marino';
$string['country.sn'] = 'Senegal';
$string['country.so'] = 'Somalia';
$string['country.sr'] = 'Suriname';
$string['country.st'] = 'Sao Tome and Principe';
$string['country.sv'] = 'El Salvador';
$string['country.sy'] = 'Siriar Arabiar Errepublika';
$string['country.sz'] = 'Swaziland';
$string['country.tc'] = 'Turkiar eta Caicos Uharteak';
$string['country.td'] = 'Chad';
$string['country.tf'] = 'Frantziar Hegoaldeko Lurraldeak';
$string['country.tg'] = 'Togo';
$string['country.th'] = 'Tailandia';
$string['country.tj'] = 'Tajikistan';
$string['country.tk'] = 'Tokelau';
$string['country.tl'] = 'Timor-leste';
$string['country.tm'] = 'Turkmenistan';
$string['country.tn'] = 'Tunisia';
$string['country.to'] = 'Tonga';
$string['country.tr'] = 'Turkia';
$string['country.tt'] = 'Trinidad eta Tobago';
$string['country.tv'] = 'Tuvalu';
$string['country.tw'] = 'Taiwan, Txinatar Probintzia';
$string['country.tz'] = 'Tanzaniar Errepublika Batua';
$string['country.ua'] = 'Ukraina';
$string['country.ug'] = 'Uganda';
$string['country.um'] = 'United States Minor Outlying Islands';
$string['country.us'] = 'Ameriketako Estatu Batuak';
$string['country.uy'] = 'Uruguai';
$string['country.uz'] = 'Uzbekistan';
$string['country.va'] = 'Vatikano Hiria';
$string['country.vc'] = 'Saint Vincent eta Grenadinak';
$string['country.ve'] = 'Venezuela';
$string['country.vg'] = 'Erresuma Batuko Birjina Uharteak';
$string['country.vi'] = 'Estatu Batuetako Birjina Uharteak';
$string['country.vn'] = 'Vietnam';
$string['country.vu'] = 'Vanuatu';
$string['country.wf'] = 'Wallis eta Futuna';
$string['country.ws'] = 'Samoa';
$string['country.ye'] = 'Yemen';
$string['country.yt'] = 'Mayotte';
$string['country.za'] = 'Hego Africa';
$string['country.zm'] = 'Zambia';
$string['country.zw'] = 'Zimbabwe';
$string['createcollect'] = 'Sortu eta bildu';
$string['createcollectsubtitle'] = 'Garatu zeure portfolioa';
$string['createyourresume'] = 'Sortu zure <a href="%s">curriculum vitaea</a>';
$string['dashboarddescription'] = 'Aginte-mahaia izeneko orria web gunean lehen aldiz sartutakoan ikusten den hasiera-orria da. Zuek baino ez duzu horretarako sarbiderik.';
$string['date'] = 'Data';
$string['dateformatguide'] = 'Erabili YYYY/MM/DD formatua';
$string['datetimeformatguide'] = 'Erabili YYYY/MM/DD HH:MM formatua';
$string['day'] = 'egun(a)';
$string['days'] = 'egunak';
$string['debugemail'] = 'OHARRA: e-posta hau %s &lt;%s&gt;(r)entzat idatzi da, baina zuri bidali zaizu &quot;sendallemailto&quot; konfigurazio-ezarpenari jarraiki.';
$string['decline'] = 'Ezetsi';
$string['default'] = 'Lehenetsi';
$string['delete'] = 'Ezabatu';
$string['deleteduser'] = 'Ezabatutako erabiltzailea';
$string['deletetag'] = 'Ezabatu <a href="%s">%s</a>';
$string['deletetagdescription'] = 'Ezabatu etiketa hau zure portfolioko elementu guztietan';
$string['description'] = 'Deskribapena';
$string['disable'] = 'Desgaitu';
$string['displayname'] = 'Izena erakutsi';
$string['divertingemailto'] = 'Desbideratu e-posta hona: %s';
$string['done'] = 'Egina';
$string['edit'] = 'Editatu';
$string['editdashboard'] = 'Editatu';
$string['editing'] = 'Editatzen';
$string['editmyprofilepage'] = 'Editatu profilaren orria';
$string['edittag'] = 'Editatu <a href="%s">%s</a>';
$string['edittagdescription'] = 'Portfolioan "%s" etiketapean dituzun elementu guztiak eguneratuko dira';
$string['edittags'] = 'Editatu etiketak';
$string['editthistag'] = 'Editatu etiketa hau';
$string['email'] = 'E-posta';
$string['emailaddress'] = 'E-posta helbidea';
$string['emailaddressorusername'] = 'E-posta helbidea edo erabiltzaile-izena';
$string['emailnotsent'] = 'Ezin izan da kontakturako e-posta bidali. Errore-mezua: &quot;%s&quot;';
$string['emailtoolong'] = 'Zure e-posta helbideak ezin du izan 255 karaktere baino gehiago';
$string['enable'] = 'Gaitu';
$string['errorprocessingform'] = 'Akatsa gertatu da formulario hau bidaltzean. Mesedez, berrikusi markatuta dauden eremuak eta saiatu berriz.';
$string['expand'] = 'Zabaldu';
$string['filenotimage'] = 'Igo duzun irudiak ez du balio. PNG, JPEG edo GIF fitxategi bat izan behar du.';
$string['fileunknowntype'] = 'Ezin izan da zehaztu zein fitxategi-mota igo duzun. Fitxategia kaltetuta egon daiteke, edo konfigurazio akats bat izan dezake. Mesedez, jarri harremanetan zure kudeatzaileekin.';
$string['filter'] = 'Iragazkia';
$string['filterresultsby'] = 'Iragazi emaitzak honen arabera:';
$string['findfriends'] = 'Bilatu lagunak';
$string['findfriendslinked'] = 'Bilatu <a href="%s">lagunak</a>';
$string['findgroups'] = 'Bilatu taldeak';
$string['first'] = 'Lehena';
$string['firstname'] = 'Izena';
$string['firstpage'] = 'Lehenengo orria';
$string['forgotpassemaildisabled'] = 'Barkatu, e-posta desgaituta dago zuk emandako e-posta helbiderako edo erabiltzaile-izenerako. Mesedez, jarri harremanetan kudeatzailearekin zure pasahitza berritzeko.';
$string['forgotpassemailsendunsuccessful'] = 'Sentitzen dugu, dirudienez e-posta mezua ezin izan da ongi bidali. Geure errua da; mesedez, saiatu berriz ere tarte baten ostean';
$string['forgotpassemailsentanyway'] = 'Erabiltzaile honentzat gordetako e-postara mezua bidali da, baina helbidea ez da zuzena edo zerbitzariak ez ditu mezuak onartzen.
Zure Mahara-ko kudeatzailearekin harremanetan jarri zure pasahitza aldatzeko mezua jasotzen ez baduzu.';
$string['forgotpassnosuchemailaddressorusername'] = 'Sartu duzun e-posta helbidea edo erabiltzaile-izena ez dator bat gune honetako erabiltzaileekin';
$string['forgotpasswordenternew'] = 'Mesedez, idatzi zure pasahitz berria aurrera egiteko';
$string['forgotusernamepassword'] = 'Ahaztu egin al duzu erabiltzaile-izena edo pasahitza?';
$string['forgotusernamepasswordemailmessagehtml'] = '<p>Kaixo %s,</p>
<p>Erabiltzaile-izena/pasahitza eskatu dute %s(e)n duzun konturako.</p> <p>>Zure erabiltzaile-izena <strong>%s</strong> da.</p> <p>Pasahitza berrezarri nahi baduzu, klikatu ondoko estekan:</p> <p><a href="%s">%s</a></p> <p>Ez baduzu eskatu pasahitza berrezartzerik, ez hartu aintzak mezu hau.</p> <p><a href="%s">%s</a></p> <p>Gai honekin lotutako zalantzarik baduzu, <a href="%s">jarri harremanetan gurekin</a>, mesedez.</p>
<p>Ondo izan,
%s Guneko kudeatzailea</p>';
$string['forgotusernamepasswordemailmessagetext'] = 'Kaixo %s,
Erabiltzaile-izena/pasahitza eskatu dute %s(e)n duzun konturako. Zure erabiltzaile izena %s da. Pasahitza berrezarri nahi baduzu, klikatu ondoko estekan: %s. Ez baduzu eskatu pasahitza berrezartzerik, ez hartu aintzat mezu hau. Gai honekin lotutako zalantzarik baduzu, jarri harremanetan gurekin, mesedez: %s
Ondo izan,
%s Guneko kudeatzailea';
$string['forgotusernamepasswordemailsubject'] = 'Erabiltzaile-izenaren/pasahitzaren xehetasunak %s(r)entzat';
$string['forgotusernamepasswordtext'] = '<p>Erabiltzaile-izena edo pasahitza ahaztu baduzu, sartu zure profilean ageri den e-posta helbidea eta mezua bidaliko dizugu; haren bidez, pasahitz berria ezarri ahal izango duzu.</p> <p>Erabiltzaile izena badakiu eta pasahitza ahaztu baduzu, erabiltzaile-izena ere sar dezakezu e-posta helbidearen ordez.</p>';
$string['formatpostbbcode'] = 'Mezua BBCode erabilita formatea dezakezu. %sIkusi gehiago%s';
$string['formerror'] = 'Errore bat gertatu da zure bildaketa burutzean. Mesedez, saiatu berriz.';
$string['formerroremail'] = 'Jarri gurekin harremanetan hemen (%s) problemak izaten jarraitzen baduzu.';
$string['fullname'] = 'Izen osoa';
$string['go'] = 'Joan';
$string['gotoinbox'] = 'Joan sarrera-ontzira';
$string['groups'] = 'Taldeak';
$string['height'] = 'Altuera';
$string['heightshort'] = 'h';
$string['hide'] = 'Ezkutatu';
$string['home'] = 'Hasiera';
$string['howtodisable'] = 'Ezkutatu egin duzu informazio-blokea. Hori ikusteko baldintzak kontral ditzakezu hemendik: <a href="%s">Ezarpenak</a>.';
$string['image'] = 'Irudia';
$string['importedfrom'] = '%s(e)tik inportatuta';
$string['inbox'] = 'Sarrera-ontzia';
$string['incomingfolderdesc'] = 'Beste sare-lan host batzuetatik inportatutako fitxategiak';
$string['installedplugins'] = 'Instalatutako pluginak';
$string['institution'] = 'Erakundea';
$string['institutionadministration'] = 'Erakunde Kudeaketa';
$string['institutionexpirywarning'] = 'Erakundeko partaidetzaren iraungitze-oharra';
$string['institutionexpirywarninghtml'] = '<p>Kaixo %s,</p> <p> %s-(e)n %s-(e)ko kide izateko epea iraungi egingo da %s barru.</p> <p>Epea luzatu nahi baduzu, edo gai honekin lotutako zalantzarik baduzu, <a href="%s">jarri harremanean gurekin</a>, mesedez.</p> <p>Ondo izan, %s Guneko Administratzailea</p>';
$string['institutionexpirywarninghtml_institution'] = '<p> %s lagun agurgarria:</p> <p>%(r)en  partaidetza %s-(e)n  %s epean etenduko da.</p> <p>Zure erakundearen partaidetza luzatu nahi baduzu edo hortaz galderarik baduzu,  <a href="%s">gurekin</a> harremanetan jar zaitezke.</p> <p>Adeitasunez, %s Guneko Kudeatzaileak</p>';
$string['institutionexpirywarninghtml_site'] = '<p>%s lagun agurgarria:</p> <p> \'%s\' erakundea %s epean etenduko da.</p> <p>Agian %s-(r)en partaidetza luzatzeko harremanetan jarri nahi duzu %s.</p> <p>Adeitasunez, %s Guneko Kudeatzaileak</p>';
$string['institutionexpirywarningtext'] = 'Kaixo %s, %s-(e)n %s-(e)ko kide izateko epea iraungi egingo da %s barru. Epea luzatu nahi baduzu, edo gai honekin lotutako zalantzarik baduzu, jarri harremanean gurekin, mesedez. Ondo izan, %s Guneko Administratzailea';
$string['institutionexpirywarningtext_institution'] = '<p>%s lagun agurgarria:</p> <p>%(r)en partaidetza %s-(e)n %s epean etenduko da. </p><p>Zure erakundearen partaidetza luzatu nahi baduzu edo hortaz galderarik baduzu,  <a href="%s">gurekin</a> harremanetan jar zaitezke.</p> <p>Adeitasunez, %s Guneko Kudeatzaileak</p>';
$string['institutionexpirywarningtext_site'] = '<p>%s lagun agurgarria:</p> <p> \'%s\' erakundea %s epean etenduko da.</p> <p>Agian harremanetan jarri nahi duzu %s-(r)en partaidetza luzatzeko.</p> <p>Adeitasunez, %s Guneko Kudeatzaileak</p>';
$string['institutionfull'] = 'Aukeratu duzun erakundeak ez du onartzen erregistro gehiago.';
$string['institutionmemberconfirmmessage'] = '%s(e)ko partaide gisa gehitu zaituzte.';
$string['institutionmemberconfirmsubject'] = 'Baieztatu erakundeko partaidetza';
$string['institutionmemberrejectmessage'] = '%s(e)ko partaide izateko eskaera ezetsi egin da.';
$string['institutionmemberrejectsubject'] = 'Erakundeari egindako partaidetza-eskaera ezetsi egin da';
$string['institutionmembership'] = 'Erakunde-partaidetza';
$string['institutionmembershipdescription'] = 'Partaide zaren erakundeen zerrenda hemen azalduko da. Erakunde bateko partaide izateko eskaera ere egin dezakezu, eta erakunde batek gonbidapenen bat bidali badizu, onartu edo ezetsi egin dezakezu.';
$string['institutionmembershipexpirywarning'] = 'Abisua erakundearen partaidetzaren iraungitze-data dela-eta';
$string['institutionmembershipexpirywarninghtml'] = '<p> %s lagun agurgarria:</p> <p>Zure  partaidetza %s-(e)n  %s epean etenduko da.</p> <p>Zure partaidetza luzatu nahi baduzu edo hortaz galderarik baduzu,  <a href="%s">gurekin</a> harremanetan jar zaitezke.</p> <p>Adeitasunez, %s Guneko Kudeatzaileak</p>';
$string['institutionmembershipexpirywarningtext'] = '<p>%s lagun agurgarria:</p> <p>Zure partaidetza %s-(e)n %s epean etenduko da. </p><p>Zure erakundearen partaidetza luzatu nahi baduzu edo hortaz galderarik baduzu,  <a href="%s">gurekin</a> harremanetan jar zaitezke.</p> <p>Adeitasunez, %s Guneko Kudeatzaileak</p>';
$string['invalidsesskey'] = 'Saio-giltza baliogabea';
$string['invitedgroup'] = 'hona gonbidatutako taldea';
$string['invitedgroups'] = 'hona gonbidatutako taldeak';
$string['itemstaggedwith'] = '"%s" etiketapean dauden elementuak';
$string['javascriptnotenabled'] = 'Zure nabigatzaileak ez du javascript gune honetarako gaituta. Maharak javascript gaituta egotea behar du bertan sartu aurretik';
$string['joingroups'] = 'Egin <a href="%s">talderen bateko partaide</a>';
$string['joininstitution'] = 'Atxiki erakundera';
$string['language'] = 'Hizkuntza';
$string['last'] = 'Azkena';
$string['lastminutes'] = 'Azken %s minutuetan';
$string['lastname'] = 'Deitura';
$string['lastpage'] = 'Azken orria';
$string['leaveinstitution'] = 'Utzi erakundea';
$string['linksandresources'] = 'Estekak eta baliabideak';
$string['loading'] = 'Kargatzen ...';
$string['loggedinusersonly'] = 'Barruan diren erabiltzaileak bakarrik';
$string['loggedoutok'] = 'Ondo irten zara';
$string['login'] = 'Sartu';
$string['loginfailed'] = 'Ez dituzu eman sartzeko datu zuzenak. Mesedez, egiaztatu zure erabiltzaile-izena eta pasahitza zuzenak direla.';
$string['loginto'] = '%s(e)n sartu';
$string['logout'] = 'Irten';
$string['lostusernamepassword'] = 'Galdutako erabiltzaile-izena/pasahitza';
$string['memberofinstitutions'] = '%s(e)ko partaidea';
$string['membershipexpiry'] = 'Partaidetzaren iraungitze-data';
$string['message'] = 'Mezua';
$string['messagesent'] = 'Zure mezua bidali da';
$string['months'] = 'hilabeteak';
$string['more...'] = 'Gehiago ...';
$string['mustspecifyoldpassword'] = 'Zure oraingo pasahitza zehaztu behar duzu';
$string['mydashboard'] = 'Nire aginte-mahaia';
$string['myfriends'] = 'Nire lagunak';
$string['mygroups'] = 'Nire taldeak';
$string['mymessages'] = 'Nire mezuak';
$string['myportfolio'] = 'Nire portfolioa';
$string['mytags'] = 'Nire etiketak';
$string['myviews'] = 'Nire bistak';
$string['name'] = 'Izena';
$string['namedfieldempty'] = 'Bete beharreko "%s" eremua hutsik dago';
$string['newpassword'] = 'Pasahitz berria';
$string['next'] = 'Hurrengoa';
$string['nextpage'] = 'Hurrengo orria';
$string['no'] = 'Ez';
$string['nocountryselected'] = 'Ez da herria aukeratu';
$string['nodeletepermission'] = 'Ez duzu tresna hau ezabatzeko baimenik';
$string['noeditpermission'] = 'Ez duzu tresna hau editatzeko baimenik';
$string['noenddate'] = 'Amaiera-datarik ez';
$string['nohelpfound'] = 'Ez da laguntzarik aurkitu item honetarako';
$string['nohelpfoundpage'] = 'Ez da laguntzarik aurkitu orri honetarako';
$string['noinstitutionsetpassemailmessagehtml'] = '<p>%Kaixo %s,</p>

<p>Aurrerantzean, ez zara %s(e)ko partaide izango.</p>
<p>%s erabiltzen jarrai dezakezu oraingo %s erabiltzaile izenarekin, baina pasahitz berria ezarri behar duzu zure konturako.</p>

<p>Mesedez, egin klik ondoko estekan berrezartze prozesuarekin jarraitzeko.</p>

<p><a href="%sforgotpass.php?key=%s">%sforgotpass.php?key=%s</a></p>

<p>Honekin lotutako zalantzarik baduzu, <a href="%scontact.php">jarri harremanetan gurekin. </a>, mesedez.</p>

<p>Ondo izan, %s Guneko kudeatzailea</p>

<p><a href="%sforgotpass.php?key=%s">%sforgotpass.php?key=%s</a></p>';
$string['noinstitutionsetpassemailmessagetext'] = 'Kaixo %s,

Aurrerantzean ez zara %s(e)ko partaide izango.
%s erabiltzen jarrai dezakezu oraingo %s erabiltzaile izenarekin, baina pasahitz berria ezarri behar duzu konturako.

Mesedez, egin klik ondoko estekan berrezartze prozesuarekin jarraitzeko.

%sforgotpass.php?key=%s

Honekin lotutako zalantzarik baduzu, jarri harremanetan gurekin, mesedez.

%scontact.php

Ondo izan, %s Guneko kudeatzailea

%sforgotpass.php?key=%s';
$string['noinstitutionsetpassemailsubject'] = '%s: %s(e)ko partaidea';
$string['none'] = 'Bat ere ez';
$string['noresultsfound'] = 'Ez da emaitzarik lortu';
$string['nosendernamefound'] = 'Igorlearen izenik ez da bidali';
$string['nosessionreload'] = 'Kargatu berriro orria sartu ahal izateko';
$string['nosuchpasswordrequest'] = 'Ez da pasahitzik eskatzen';
$string['notifications'] = 'Jakinarazpenak';
$string['notinstallable'] = 'Ezin da instalatu!';
$string['notinstalledplugins'] = 'Pluginak ez dira instalatu';
$string['notphpuploadedfile'] = 'Igotzeko prozesuan fitxategia galdu egin da. Horrek ez luke gertatu behar; mesedez, jarri harremanetan zure kudeatzaileekin informazio gehiago lortzeko.';
$string['numitems'] = '%s elementu';
$string['oldpassword'] = 'Oraingo pasahitza';
$string['onlineusers'] = 'Online erabiltzaileak';
$string['optionalinstitutionid'] = 'Erakundearen ID (aukerakoa)';
$string['organisedescription'] = 'Antolatu portfolioa <a href="%s">orriak</a> erabilita. Sortu orri ezberdinak hartzaile ezberdinentzat - horietako bakoitzean aukeratu ahal duzu zein elementu gehitu.';
$string['organisesubtitle'] = 'Erakutsi orri ezberdinak dituen portfolioa';
$string['password'] = 'Pasahitza';
$string['passwordchangedok'] = 'Pasahitza ondo aldatu da';
$string['passwordhelp'] = 'Sistemara sartzeko erabiltzen duzun pasahitza';
$string['passwordnotchanged'] = 'Ez duzu pasahitza aldatu; mesedez, aukeratu pasahitz berri bat';
$string['passwordsaved'] = 'Pasahitz berria gorde da';
$string['passwordsdonotmatch'] = 'Pasahitza ez dator bat';
$string['passwordtooeasy'] = 'Pasahitza errazegia da! Mesedez, aukeratu zailago bat';
$string['pendingfriend'] = 'onartzeke dagoen laguna';
$string['pendingfriends'] = 'onartzeke dauden lagunak';
$string['phpuploaderror'] = 'Errorea gertatu da fitxategia igotzean: %s (%s errore-kodea)';
$string['phpuploaderror_1'] = 'Igotako fitxategiak gainditu egin du php.ini-n zehaztutako gehienezko tamaina (upload_max_filesize zuzentaraua).';
$string['phpuploaderror_2'] = 'Igotako fitxategiak gainditu egin du HTML formatuan zehaztutako gehienezko tamaina (MAX_FILE_SIZE zuzentaraua).';
$string['phpuploaderror_3'] = 'Fitxategiaren zati bat igo da soilik.';
$string['phpuploaderror_4'] = 'Ez da fitxategirik igo.';
$string['phpuploaderror_6'] = 'Aldi baterako karpeta falta da.';
$string['phpuploaderror_7'] = 'Ezin izan da fitxategia diskoan idatzi.';
$string['phpuploaderror_8'] = 'Luzapenak fitxategia igotzeari uztea eragin du.';
$string['pleasedonotreplytothismessage'] = 'Ez erantzun mezu honi, mesedez.';
$string['plugindisabled'] = 'Plugina desgaituta';
$string['pluginenabled'] = 'Plugina gaituta';
$string['pluginexplainaddremove'] = 'Maharako pluginak beti instalatuta daude eta erabiltzaileak sar daitezke URLak ezagutuz gero. Funtzioak aktibatu edo desaktibatu baino, izkutuan edo ikusgai jar daitezke plugin orriko behealdeko "Ezkutatu" eta "Erakutsi" esteketan klik eginez.';
$string['pluginexplainartefactblocktypes'] = '\'Tresna\' moduko plugin bat ezkutatuz, Mahara sistemak horrekin lotutako blokeak erakusteari ere uzten dio.';
$string['pluginnotenabled'] = 'Plugina ez dago gaituta. Lehenago %s plugina gaitu behar duzu.';
$string['plugintype'] = 'Plugin-mota';
$string['preferences'] = 'Hobespenak';
$string['preferredname'] = 'Erakusteko izena';
$string['previous'] = 'Aurrekoa';
$string['prevpage'] = 'Aurreko orria';
$string['primaryemailinvalid'] = 'Zure lehenetsitako e-posta okerra da';
$string['privacystatement'] = 'Pribatutasun-adierazpena';
$string['processing'] = 'Prozesatzen';
$string['profile'] = 'profila';
$string['profiledescription'] = 'Zure profila agertuko da zure izenaren gainean edo profileko irudiaren gainean klik eginez gero';
$string['profileimage'] = 'Profil-irudia';
$string['publishablog'] = 'Argitaratu <a href="%s">egunkaria</a>';
$string['pwchangerequestsent'] = 'Laster, e-posta bat jasoko duzu kontuaren pasahitza aldatzeko erabili dezakezun estekarekin.';
$string['quarantinedirname'] = 'berrogeialdia';
$string['query'] = 'Kontsulta';
$string['querydescription'] = 'Bilatu nahi diren hitzak';
$string['quota'] = 'Kuota';
$string['quotausage'] = '<span id="quota_total">%s</span> kuotaren <span id="quota_used">%s</span> erabili duzu.';
$string['reallyleaveinstitution'] = 'Ziur al zaude erakunde honetatik irten nahi duzula?';
$string['reason'] = 'Arrazoia';
$string['recentactivity'] = 'Azken orduko jarduera';
$string['register'] = 'Erregistratu';
$string['registeragreeterms'] = 'Onartu egin behar dituzu <a href="terms.php">arau eta baldintzak</a>.';
$string['registeringdisallowed'] = 'Sentitzen dugu, oraingoan ezin duzu sisteman erregistratu';
$string['registerprivacy'] = 'Hemen bildutako datuak gure <a href="privacy.php">pribatutasun-adierazpenaren</a> arabera gordeko dira.';
$string['registerstep1description'] = 'Ongi etorri! Gune hau erabiltzeko erregistratu egin behar zara, eta <a href="terms.php">arau eta baldintzak</a> onartu behar dituzu. Hemen jasotzen diren datuak <a href="privacy.php">pribatutasun-adierazpena</a> aintzat hartuta gordeko dira</a>.';
$string['registerstep3fieldsmandatory'] = '<h3>Bete beharreko profil-eremuak</h3><p>Ondoko eremuak bete beharrekoak dira. Zure erregistroa osatu ahal izateko bete egin behar dituzu.</p>';
$string['registerstep3fieldsoptional'] = '<h3>Aukeratu profil-Irudi bat.</h3><p>Dagoeneko erregistratuta zaude %s(e)n! Orain,  profil-irudi bat aukera dezakezu avatar gisa erakusteko.</p>';
$string['registerwelcome'] = 'Ongi etorri! Gune hau erabiltzeko, lehenago erregistratu behar duzu.';
$string['registrationcomplete'] = 'Eskerrik asko %s(e)n erregistratzeagatik';
$string['registrationnotallowed'] = 'Aukeratu duzun erakundeak ez du onartzen  norbere burua erregistratzerik.';
$string['reject'] = 'Ezetsi';
$string['remotehost'] = '%s urruneko ostalaria';
$string['remove'] = 'Ezabatu';
$string['republish'] = 'Argitaratu';
$string['requestmembershipofaninstitution'] = 'Erakunde bati partaide izateko eskaera egin';
$string['requiredfieldempty'] = 'Bete beharreko eremu bat hutsik dago';
$string['result'] = 'emaitza';
$string['results'] = 'emaitza';
$string['returntosite'] = 'Itzuli gunera';
$string['save'] = 'Gorde';
$string['search'] = 'Bilatu';
$string['searchresultsfor'] = 'Bilatu emaitzak honetarako';
$string['searchusers'] = 'Bilatu erabiltzaileak';
$string['select'] = 'Aukeratu';
$string['selectatagtoedit'] = 'Aukera ezazu etiketa bat editatzeko';
$string['selfsearch'] = 'Bilatu nire portfolioa';
$string['send'] = 'Bidali';
$string['sendmessage'] = 'Bidali mezua';
$string['sendrequest'] = 'Bidali eskaera';
$string['sessiontimedout'] = 'Saioa denboraz kanpo geratu da. Mesedez, sartu sarrerako datuak jarraitu ahal izateko.';
$string['sessiontimedoutpublic'] = 'Saioa denboraz kanpo geratu da. <a href="?login">Sartu</a> jarraitu ahal izateko';
$string['sessiontimedoutreload'] = 'Saioa denboraz kanpo geratu da. Kargatu berriz orrialdea sartu ahal izateko';
$string['setblocktitle'] = 'Blokeari izenburua eman';
$string['settings'] = 'Ezarpenak';
$string['settingssaved'] = 'Ezarpenak gorde dira';
$string['settingssavefailed'] = 'Ezin izan dira gorde ezarpenak';
$string['sharenetwork'] = 'Partekatu eta sareak';
$string['sharenetworkdescription'] = 'Zehaztasun osoz ezarri ahal duzu nork izango duen zure orrirako sarbidea, eta zenbat denboraz.';
$string['sharenetworksubtitle'] = 'Bildu lagunekin eta taldeetako partaide egin';
$string['show'] = 'Erakutsi';
$string['showtags'] = 'Erakutsi nire etiketak';
$string['siteadministration'] = 'Gunearen kudeaketa';
$string['siteclosed'] = 'Gune hau itxi egin da datu-basea eguneratu bitartean. Guneko kudeatzaileak soilik sar daitezke.';
$string['siteclosedlogindisabled'] = 'Gune hau itxi egin da datu-basea eguneratu bitartean. <a href="%s">Eguneratu orain.</a>';
$string['sitecontentnotfound'] = '%s testua ez dago erabilgarri';
$string['sizeb'] = 'b';
$string['sizegb'] = 'GB';
$string['sizekb'] = 'KB';
$string['sizemb'] = 'MB';
$string['sortalpha'] = 'Ordenatu etiketak alfabetikoki';
$string['sortfreq'] = 'Ordenatu etiketak maiztasunaren arabera';
$string['sortresultsby'] = 'Ordenatu emaitzak honen arabera:';
$string['spamtrap'] = 'Spam tranpa';
$string['strftimenotspecified'] = 'Zehaztugabea';
$string['studentid'] = 'ID zenbakia';
$string['subject'] = 'Gaia';
$string['submit'] = 'Bidali';
$string['system'] = 'Sistema';
$string['tagdeletedsuccessfully'] = 'Etiketa ondo ezabatu da';
$string['tagfilter_all'] = 'Guztiak';
$string['tagfilter_file'] = 'Fitxategiak';
$string['tagfilter_image'] = 'Irudiak';
$string['tagfilter_text'] = 'Testua';
$string['tagfilter_view'] = 'Orriak';
$string['tags'] = 'Etiketak';
$string['tagsdesc'] = 'Idatzi elementu honetarako etiketak komaz banatuta';
$string['tagsdescprofile'] = 'Idatzi elementu honetarako etiketak komaz banatuta. \'Profila\' etiketa duten elementuak alboko barran erakutsiko dira.';
$string['tagupdatedsuccessfully'] = 'Etiketa ondo eguneratu da';
$string['termsandconditions'] = 'Arauak eta baldintzak';
$string['theme'] = 'Itxura';
$string['thisistheprofilepagefor'] = 'Hau da %s(r)en profil-orria';
$string['topicsimfollowing'] = 'Jarraitzen ditudan gaiak';
$string['unknownerror'] = 'Errore ezezagun bat gertatu da (0x20f91a0)';
$string['unreadmessage'] = 'irakurri gabeko mezua';
$string['unreadmessages'] = 'irakurri gabeko mezuak';
$string['update'] = 'Eguneratu';
$string['updatefailed'] = 'Eguneraketak kale egin du';
$string['updateyourprofile'] = 'Eguneratu <a href="%s">zeure profila</a>';
$string['upload'] = 'Igo';
$string['uploadedfiletoobig'] = 'Fitxategia handiegia da. Mesedez galdetu zure kudeatzaileei informazio gehiago lortzeko.';
$string['uploadyourfiles'] = 'Igo <a href="%s">fitxategiak</a>';
$string['useradministration'] = 'Erabiltzaileen kudeaketa';
$string['username'] = 'Erabiltzaile-izena';
$string['usernamehelp'] = 'Sisteman sartzeko eman duzun erabiltzaile-izena.';
$string['users'] = 'Erabiltzaileak';
$string['usersdashboard'] = '%s(r)en aginte-mahaia';
$string['usersprofile'] = '%s(r)en profila';
$string['view'] = 'Orria';
$string['viewmyprofilepage'] = 'Profilaren orria ikusi';
$string['views'] = 'Orriak';
$string['virusfounduser'] = 'Antibirusak igo duzun fitxategia, %s, eskaneatu du eta infektatuta dagoela ikusi du! Zure fitxategia EZ da igo.';
$string['virusrepeatmessage'] = '%s erabiltzaileak zenbait fitxategi igo ditu, antibirusak eskaneatu egin ditu eta infektatuta daudela ikusi du.';
$string['virusrepeatsubject'] = 'Kontuz: %s(e)k behin eta berriz igotzen du birusa.';
$string['weeks'] = 'asteak';
$string['width'] = 'Zabalera';
$string['widthshort'] = 'z';
$string['years'] = 'adina';
$string['yes'] = 'Bai';
$string['youareamemberof'] = '%s(e)ko partaidea zara';
$string['youaremasqueradingas'] = '%s bezala mozorrotuta zaude';
$string['youhavebeeninvitedtojoin'] = '%s(e)n sartzera gonbidatu zaituzte';
$string['youhavenottaggedanythingyet'] = 'Ez duzu ezer etiketatu oraindik';
$string['youhaverequestedmembershipof'] = 'Partaidetza-eskaera egina duzu %s(e)n.';
$string['youraccounthasbeensuspended'] = 'Zure kontua etenda dago';
$string['youraccounthasbeensuspendedreasontext'] = '%s(e)k eten egin du %(e)n duzun kontua. Arrazoia: %s';
$string['youraccounthasbeensuspendedtext2'] = '%s(e)k eten egin du %(e)n duzun kontua.';
$string['youraccounthasbeenunsuspended'] = 'Zure kontua berriro aktibatu da';
$string['youraccounthasbeenunsuspendedtext2'] = '%s(e)n duzun kontua berriro aktibatu da. Berriro sar zaitezke eta gunea erabili.';
$string['yournewpassword'] = 'Pasahitz berria';
$string['yournewpasswordagain'] = 'Errepikatu pasahitz berria';
?>
