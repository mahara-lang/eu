<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['addtowatchlist'] = 'Gehitu kontrol-zerrendan.';
$string['adminnotificationerror'] = 'Erabiltzailearen errore-jakinarazpena ziurrenik zure zerbitzariaren konfigurazioak sortu du.';
$string['alltypes'] = 'Mota guztiak';
$string['artefacts'] = 'Tresnak';
$string['attime'] = '-(e)an';
$string['date'] = 'Data';
$string['deleteallnotifications'] = 'Ezabatu jakinarazpen guztiak';
$string['deletednotifications'] = '%s oharrak ezabatuta';
$string['failedtodeletenotifications'] = 'Ezin izan dira oharrak ezabatu';
$string['failedtomarkasread'] = 'Oharrak ezin izan dira \'irakurrita\' bezala markatu';
$string['groups'] = 'Taldeak';
$string['institutioninvitemessage'] = 'Erakunde honen partaide zarela egiazta dezakezu zure Erakundearen Ezarpen Orrian:';
$string['institutioninvitesubject'] = '%s erakundean parte hartzera gonbidatu zaituzte.';
$string['institutionrequestmessage'] = 'Erabiltzaileak erakundeetara gehitu ahal dituzu \'Erakundeko Erabiltzaileak\' izeneko orrian.';
$string['institutionrequestsubject'] = '%s(e)k %s(e)n parte hartzea eskatu du.';
$string['markasread'] = 'Irakurritakotzat markatu';
$string['markedasread'] = 'Oharrak \'irakurrita\' bezala markatu';
$string['missingparam'] = 'Eskatutako %s parametroa hutsik dago %s motako jarduerarako.';
$string['monitored'] = 'Monitorizatua';
$string['newcontactus'] = 'Kontaktu berria';
$string['newcontactusfrom'] = 'Kontaktu berria';
$string['newfeedbackonartefact'] = 'Feedback berria gailuan';
$string['newfeedbackonview'] = 'Feedback berria bistan';
$string['newgroupmembersubj'] = '%s taldekidea da orain!';
$string['newviewaccessmessage'] = '%s(e)k sarreradunen zerrendara gehitu zaitu ondoko orrian: "%s"';
$string['newviewaccessmessagenoowner'] = 'Sarreradunen zerrendara gehituta zaude ondoko orrian: "%s"';
$string['newviewaccesssubject'] = 'Orri berrirako sarbidea';
$string['newviewmessage'] = '%s(e)k "%s" izeneko orria sortu du';
$string['newviewsubject'] = 'Orri berria sortu da';
$string['newwatchlistmessage'] = 'Ez dago jarduerarik zure kontrol-zerrendan.';
$string['newwatchlistmessageview'] = '%s(e)k bere "%s" orria aldatu du.';
$string['objectionablecontentartefact'] = '%s(e)k eduki desegokia salatu du "%s" gailuan.';
$string['objectionablecontentview'] = '"%s" orrian %s(e)k eduki desegokia salatu du';
$string['objectionablecontentviewartefact'] = '%s(e)k eduki desegokia salatu du "%s"-n "%s" orrian';
$string['objectionablecontentviewartefacthtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;">Eduki desegokia "%s"-n,  "%s"-n  %s-k bidalia<strong></strong><br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p>Honekin lotuta dago salaketa: <a href="%s">%s</a></p> <p>Honek bidalia: <a href="%s">%s</a></p> </div>';
$string['objectionablecontentviewartefacttext'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;">Eduki desegokia "%s"-n,  "%s"-n  %s-k bidalia<strong></strong><br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"><p> Orria ikusteko, erabili esteka hau: %s </p><p>Bidaltzailaren profila ikusteko, erabili esteka hau: %s</p></div>';
$string['objectionablecontentviewhtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;">Eduki desegokia "%s"-n,  "%s"-n  %s-k bidalia<strong></strong><br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p>Honekin lotuta dago salaketa: <a href="%s">%s</a></p> <p>Honek bidalia: <a href="%s">%s</a></p> </div>';
$string['objectionablecontentviewtext'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;">Eduki desegokia "%s"-n,  "%s"-n  %s-k bidalia<strong></strong><br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"><p> Orria ikusteko, erabili esteka hau: %s </p><p>Bidaltzailaren profila ikusteko, erabili esteka hau: %s</p></div>';
$string['ongroup'] = 'taldean';
$string['ownedby'] = 'jabea';
$string['prefsdescr'] = 'Ez baduzu email-aukerarik hartzen, oharrak Jarduera-erregistroan azalduko dira berdin-berdin, baina automatikoki markatuko dira irakurrita bezala.';
$string['read'] = 'Irakurri';
$string['reallydeleteallnotifications'] = 'Ziur al zaude zure jakinarazpen guztiak ezabatu nahi dituzula?';
$string['recurseall'] = 'Egin dena errekurtsibo';
$string['removedgroupmembersubj'] = '%s ez da taldekidea jada';
$string['removefromwatchlist'] = 'Ezabatu kontrol-zerrendatik';
$string['selectall'] = 'Aukeratu guztia';
$string['stopmonitoring'] = 'Gelditu monitorizazioa';
$string['stopmonitoringfailed'] = 'Ezin izan da monitorizazioa gelditu';
$string['stopmonitoringsuccess'] = 'Monitarizazioa ondo gelditu da';
$string['subject'] = 'Gaia';
$string['type'] = 'Jarduera-mota';
$string['typeadminmessages'] = 'Kudeaketa-mezuak';
$string['typecontactus'] = 'Jarri harremanean gurekin';
$string['typefeedback'] = 'Feedback-a';
$string['typegroupmessage'] = 'Taldearen mezua';
$string['typeinstitutionmessage'] = 'Erakundearen mezua';
$string['typemaharamessage'] = 'Sistemaren mezua';
$string['typeobjectionable'] = 'Eduki desegokia';
$string['typeusermessage'] = 'Beste erabiltzaileengandik jasotako mezua';
$string['typeviewaccess'] = 'Orri berrira sartu';
$string['typevirusrelease'] = 'Birus-marka askatu';
$string['typevirusrepeat'] = 'Birus-eguneratzea errepikatu';
$string['typewatchlist'] = 'Kontrol-zerrenda';
$string['unread'] = 'Irakurri gabe';
$string['viewmodified'] = 'orria aldatu dute';
$string['views'] = 'Bistak';
$string['viewsandartefacts'] = 'Bistak eta gailuak';
$string['viewsubmittedmessage'] = '%s(e)k bere "%s" orriari %s(r)i bidali dio';
$string['viewsubmittedsubject'] = 'Orria %s(r)i bidalita';
?>
