<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['addauthority'] = 'Gehitu agintaria';
$string['application'] = 'Aplikazioa';
$string['authloginmsg'] = 'Sartu mezu bat erabiltzaileari erakusteko Mahara-ren bidez sartzen saiatzen denean.';
$string['authname'] = 'Agintari-izena';
$string['cannotremove'] = 'Ezin dugu auth plugin hau ezabatu, erakunde honentzat dagoen plugin bakarra da eta.';
$string['cannotremoveinuse'] = 'Ezin dugu autentikazio-plugin hau ezabatu, erabiltzen ari baitira.
Plugin hau ezabatu aurretik gordeta dituzten fitxategiak eguneratu behar dituzu.';
$string['cantretrievekey'] = 'Errorea gertatu da giltza publikoa urrutiko zerbitzaritik berreskuratzean.<br>Mesedez, ziurtatu Aplikazioa eta WWW Root-eremuak zuzenak direla, eta sare-lana gaituta dagoela urrutiko ostalarian.';
$string['changepasswordurl'] = 'Pasahitza aldatzeko URLa';
$string['duplicateremoteusername'] = 'Erabiltzaile-izen hau dagoeneko %s erabiltzaileak erabili du kanpoko autentifikaziorako. Kanpoko autentifikaziorako izenak bakarra izan behar du autentifikazio-metodo bakoitzerako.';
$string['duplicateremoteusernameformerror'] = 'Kanpoko autentifikaziorako erabiltzailek-izenak bakarra izan behar du autentifikazio-metodo bakoitzerako.';
$string['editauthority'] = 'Editatu agintaria';
$string['errnoauthinstances'] = 'Ez du ematen autentifikazio-pluginen instantziarik dugunik konfiguratuta %s(e)ko ostalarirako.';
$string['errnoxmlrpcinstances'] = 'Ez du ematen XMLRPC autentifikazio-pluginen instantziarik dugunik konfiguratuta %s(e)ko ostalarirako.';
$string['errnoxmlrpcuser'] = 'Ezin izan duzu zeure burua identifikatu oraingoan. Arrazoiak honakoak izan daitezke: *SSO-saioa iraungi egin da. Itzuli beste aplikaziora eta klikatu atzera Mahara-rako estekan. *Ez duzu baimenik SSO bidez sartzeko Mahara-n. Mesedez, egiaztatu hau zure kudeatzailearekin baimena izan beharko zenukeela uste baduzu.';
$string['errnoxmlrpcwwwroot'] = 'Ez dugu erregistratuta inolako ostalaririk %s(e)n.';
$string['errorcertificateinvalidwwwroot'] = 'Egiaztagiri honek %s(e)rako dela dio, baina zu %s(e)rako erabiltzen saiatzen ari zara.';
$string['errorcouldnotgeneratenewsslkey'] = 'Ezin da beste SSL gakorik sortu. Ziur al zaude bai openssl eta bai openssl-rako PHP modulua instalatuta daudela makina honetan?';
$string['errornotvalidsslcertificate'] = 'SSL agiriak ez du balio';
$string['host'] = 'Ostalariaren izena edo helbidea';
$string['hostwwwrootinuse'] = 'Dagoeneko beste erakunde bat (%s) ari da erabiltzen WWW root-a';
$string['ipaddress'] = 'IP helbidea';
$string['name'] = 'Gunearen izena';
$string['noauthpluginconfigoptions'] = 'Ez dago konfigurazio-aukerarik plugin honekin lotuta';
$string['nodataforinstance'] = 'Ezin izan da daturik aurkitu autentifikazio-instantziarako';
$string['parent'] = 'Aginte nagusia';
$string['port'] = 'Atakaren zenbakia';
$string['protocol'] = 'Protokoloa';
$string['requiredfields'] = 'Eskatutako profil-eremuak';
$string['requiredfieldsset'] = 'Eskatutako profil-eremuen multzoa';
$string['saveinstitutiondetailsfirst'] = 'Mesedez, gorde erakundearen xehetasunak autentikazio-pluginak konfiguratu aurretik.';
$string['shortname'] = 'Zure gunearen izen laburra';
$string['ssodirection'] = 'SSo helbidea';
$string['theyautocreateusers'] = 'Beraiek sortzen dituzte erabiltzaileak';
$string['theyssoin'] = 'SSOrekin sartuta';
$string['unabletosigninviasso'] = 'Ezinezko SSO bidez sartzea';
$string['updateuserinfoonlogin'] = 'Sartzean, eguneratu erabiltzailearen informazioa';
$string['updateuserinfoonlogindescription'] = 'Erabiltzailearen informazioa berreskuratu urruneko zerbitzaritik eta eguneratu gordetako informazio lokala erabiltzailea sistemara sartzen den bakoitzean.';
$string['weautocreateusers'] = 'Geuk sortzen ditugu erabiltzaileak';
$string['weimportcontent'] = 'Edukia inportatzen dugu (zenbait aplikazio)';
$string['weimportcontentdescription'] = '(hainbat aplikaziok bakarrik)';
$string['wessoout'] = 'SSOtik aterata';
$string['wwwroot'] = 'WWW root';
$string['xmlrpccouldnotlogyouin'] = 'Sentitzen dugu; ezin izan dizugu sarrera eman :(';
$string['xmlrpccouldnotlogyouindetail'] = 'Sentitzen dugu; ezin izan dizugu sarrera eman Mahara-n orain. Mesedez, saiatu tarte baten ondoren, eta arazoak jarraitzen badu, kontaktatu zure kudeatzailearekin.';
$string['xmlrpcserverurl'] = 'XML-RPC zerbitzaria URL';
?>
