<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['Collection'] = 'Bilduma';
$string['Collections'] = 'Bildumak';
$string['about'] = 'Honi buruz';
$string['access'] = 'Sarbidea';
$string['accesscantbeused'] = 'Sarbide-ukapena ez da gorde. Aukeratutako orri-sarbidea (ezkutuko URL-a) ezin da erabili hainbat orrirako.';
$string['accessignored'] = 'Ezkutuko URL bidezko sarbide-mota batzuk baztertu dira';
$string['accessoverride'] = 'Baliogabetutako sarbidea';
$string['accesssaved'] = 'Bildumarako sarbidea ondo gorde da';
$string['add'] = 'Gehitu';
$string['addviews'] = 'Gehitu orriak';
$string['addviewstocollection'] = 'Gehitu orriak bildumara';
$string['back'] = 'Atzera';
$string['cantdeletecollection'] = 'Ezin duzu bilduma hau ezabatu.';
$string['canteditdontown'] = 'Bilduma hau ezin duzu editatu jabea ez zarelako.';
$string['collection'] = 'bilduma';
$string['collectionaccess'] = 'Bildumarako sarbidea';
$string['collectionaccesseditedsuccessfully'] = 'Bildumarako sarbidea ondo gorde da';
$string['collectionconfirmdelete'] = 'Bilduma honetako orriak ez dira ezabatuko. Ziur al zaude bilduma ezabatu nahi duzula?';
$string['collectioncreatedsuccessfully'] = 'Bilduma ondo sortu da.';
$string['collectioncreatedsuccessfullyshare'] = 'Zure bilduma arrakastaz sortu da. Behealdeko estekaz baliatuz zure bilduma beste erabiltzaileekin partekatu.';
$string['collectiondeleted'] = 'Bilduma ondo ezabatu da.';
$string['collectiondescription'] = 'Bilduma beste batekin lotuta dagoen Orri-multzoa da eta sarbide baimen bera du. Nahi beste bilduma sor dezakezu, baina orri batek ezin du bilduma batean baino gehiagotan agertu.';
$string['collectioneditaccess'] = '%d orritarako sarbidea editatzen ari zara bilduma honetan';
$string['collections'] = 'Bildumak';
$string['collectionsaved'] = 'Bilduma ondo gorde da';
$string['confirmcancelcreatingcollection'] = 'Bilduma hau ez da osatu. Benetan utzi nahi al duzu?';
$string['created'] = 'Sortuta';
$string['deletecollection'] = 'Ezabatu bilduma';
$string['deletespecifiedcollection'] = 'Ezabatu \'%s\' bilduma';
$string['deleteview'] = 'Orria bildumatik ezabatu';
$string['deletingcollection'] = 'Bilduma ezabatzen';
$string['description'] = 'Bildumaren deskribapena';
$string['editaccess'] = 'Editatu bildumarako sarbidea';
$string['editcollection'] = 'Editatu bilduma';
$string['editingcollection'] = 'Bilduma editatzen';
$string['edittitleanddesc'] = 'Editatu izenburua eta deskribapena';
$string['editviewaccess'] = 'Editatu orriaren sarbidea';
$string['editviews'] = 'Editatu bildumaren orriak';
$string['emptycollection'] = 'Bilduma hutsik dago';
$string['emptycollectionnoeditaccess'] = 'Ezin duzu bilduma hutsetarako sarbidea editatu. Gehitu lehenago orriren bat.';
$string['incorrectaccesstype'] = 'Kontuz: \'URL sekretua\' motako sarbidea ez dago sarbide-mota orokor gisa ezartzeko moduan. Hori baztertu eta indarrean dauden bistetarako sarbideak mantendu egingo dira. Hala ere, aukeratutako bista orokorrak beste sarbide mota batzuk baditu horiek erabiliko dira eta dauden bistetarako sarbide-motak kendu.';
$string['manageviews'] = 'Kudeatu orriak';
$string['mycollections'] = 'Nire bildumak';
$string['name'] = 'Bildumaren izena';
$string['newcollection'] = 'Bilduma berria';
$string['nocollectionsaddone'] = 'Ez dago bildumarik. %sGehitu bat%s!';
$string['nooverride'] = 'Ez baliogabetu';
$string['nooverridesaved'] = 'Ez baliogabetu sarbidea aukeratutako bistetara';
$string['noviews'] = 'Ez dago orririk.';
$string['noviewsaddsome'] = 'Ez dago orririk bilduman. %sGehitu baten bat!%s';
$string['noviewsavailable'] = 'Ez dago gehitzeko orririk.';
$string['overrideaccess'] = 'Baliogabetu sarbidea';
$string['pluginname'] = 'Bildumak';
$string['potentialviews'] = 'Balizko orriak';
$string['saveapply'] = 'Aplikatu &amp; Gorde';
$string['savecollection'] = 'Gorde bilduma';
$string['update'] = 'Eguneratu';
$string['usecollectionname'] = 'Bildumaren izena erabili?';
$string['usecollectionnamedesc'] = 'Blokearen izenburuan agertzen den bildumaren izena erabili nahi baduzu utzi hau markatuta.';
$string['viewaddedtocollection'] = 'Orria bildumara gehituta. Bilduma eguneratuta orri berrirako sarbidea gehitzeko.';
$string['viewcollection'] = 'Ikusi bildumaren xehetasunak';
$string['viewconfirmremove'] = 'Ziur al zaude orri hau bildumatik ezabatu nahi duzula?';
$string['viewcount'] = 'Orriak';
$string['viewnavigation'] = 'Ikusi nabigazio-barra';
$string['viewnavigationdesc'] = 'Gehitu berez nabigazio-barra horizontala bilduma honetako orri guztietarako.';
$string['viewremovedsuccessfully'] = 'Orria ondo ezabatu da';
$string['views'] = 'Bistak';
$string['viewsaddedtocollection'] = 'Orriak bildumara gehituta. Bilduma eguneratuta orri berrietarako sarbidea gehitzeko.';
$string['viewstobeadded'] = 'Gehitzeko orriak';
?>
