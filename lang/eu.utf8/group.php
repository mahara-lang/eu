<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['About'] = 'Honi buruz';
$string['Admin'] = 'Kudeatzailea';
$string['Created'] = 'Sorrera';
$string['Files'] = 'Fitxategiak';
$string['Friends'] = 'Lagunak';
$string['Group'] = 'Taldea';
$string['Joined'] = 'Izena emanda';
$string['Members'] = 'Partaideak';
$string['Public'] = 'Publikoa';
$string['Reply'] = 'Erantzuna';
$string['Role'] = 'Rola';
$string['Type'] = 'Mota';
$string['Views'] = 'Orriak';
$string['aboutgroup'] = '%s(r)i buruz';
$string['acceptinvitegroup'] = 'Onartu';
$string['addedtofriendslistmessage'] = '%s(e)k lagun gisa gehitu zaitu! Ondorioz, %s ere zure lagun-zerrendan gehituko da.  Klikatu jarraian dagoen loturan bere profil-orrialdea ikusteko.';
$string['addedtofriendslistsubject'] = 'Lgun berria';
$string['addedtogroupmessage'] = '%s(e)k talde batera gehitu zaitu, \'%s\'.  Klikatu jarraian dagoen loturan taldea ikusteko';
$string['addedtogroupsmessage'] = '%s(e)k talde batera/batzuetara gehitu zaitu: %s';
$string['addedtogroupsubject'] = 'Talde batera gehitu zaituzte';
$string['addmembers'] = 'Gehitu partaideak';
$string['addnewinteraction'] = 'Gehitu %s berria';
$string['addtofriendslist'] = 'Gehitu lagunetara';
$string['addtomyfriends'] = 'Gehitu Nire Lagunak eremuan!';
$string['adduserfailed'] = 'Erabiltzailea ez da gehitu';
$string['addusertogroup'] = 'Gehitu';
$string['allcategories'] = 'Kategoria guztiak';
$string['allfriends'] = 'Lagun guztiak';
$string['allgroupmembers'] = 'Taldekide guztiak';
$string['allgroups'] = 'Talde guztiak';
$string['allmygroups'] = 'Nire Talde guztiak';
$string['allowssubmissions'] = 'Baimendu bidalketak';
$string['alreadyfriends'] = 'Dagoeneko %s-ren laguna zara';
$string['approve'] = 'Onartu';
$string['approverequest'] = 'Eskaera onartu!';
$string['backtofriendslist'] = 'Bueltatu Lagunen Zerrendara';
$string['cannotinvitetogroup'] = 'Ezin duzu erabiltzaile hau zure taldera gonbidatu';
$string['cannotrequestfriendshipwithself'] = 'Ezin diozu laguntasun eskaerarik bidali zeure buruari';
$string['cannotrequestjoingroup'] = 'Ezin duzu talde honetan sartzeko eskaerarik egin';
$string['cantdeletegroup'] = 'Ezin duzu talde hau ezabatu';
$string['cantdenyrequest'] = 'Hau ez da baliozko laguntasun-eskaera';
$string['canteditdontown'] = 'Ezin duzu talde hau editatu ez delako zure jabetzapekoa';
$string['cantleavegroup'] = 'Ezin duzu talde hau utzi';
$string['cantmessageuser'] = 'Ezin diozu mezurik bidali erabiltzaile honi';
$string['cantremovefriend'] = 'Ezin duzu erabiltzaile hau zure lagun-zerrendatik ezabatu';
$string['cantremovemember'] = 'Tutoreak ezin ditu partaideak kendu.';
$string['cantremoveuserisadmin'] = 'Tutoreak ezin ditu kendu kudeatzaileak eta beste tutoreak';
$string['cantrequestfriendship'] = 'Ezin diozu laguntasun-eskaerarik bidali erabiltzaile honi';
$string['cantrequestfrienship'] = 'Ezin duzu erabiltzaile honen laguntasun-eskaerarik jaso';
$string['cantviewmessage'] = 'Ezin duzu mezu hau ikusi';
$string['categoryunassigned'] = 'Kategoria esleitu gabeak';
$string['changedgroupmembership'] = 'Taldekideak onod eguneratu dira.';
$string['changedgroupmembershipsubject'] = 'Zure taldekideak aldatu dira';
$string['changerole'] = 'Aldatu rola';
$string['changerolefromto'] = 'Aldatu %s(r)en rola beste honetara:';
$string['changeroleofuseringroup'] = 'Aldatu %s(r)en rola %s(en)n';
$string['changeroleto'] = 'Aldatu rola hona';
$string['confirmremovefriend'] = 'Ziur al zaude erabiltzaile hau zure lagun-zerrendatik ezabatu nahi duzula?';
$string['couldnotjoingroup'] = 'Ezin zara talde honetan sartu';
$string['couldnotleavegroup'] = 'Ezin duzu talde hau utzi';
$string['couldnotrequestgroup'] = 'Ezin duzu partaide izateko eskaerarik bidali';
$string['creategroup'] = 'Sortu taldea';
$string['current'] = 'Oraingoa';
$string['currentfriends'] = 'Oraingo lagunak';
$string['currentrole'] = 'Oraingo rola';
$string['declineinvitegroup'] = 'Ezetsi';
$string['declinerequest'] = 'Ezetsi eskaera';
$string['declinerequestsuccess'] = 'Taldekidea izateko eskaera ondo ezetsi da.';
$string['deletegroup'] = 'Taldea ondo ezabatu da';
$string['deletegroup1'] = 'Ezabatu taldea';
$string['deleteinteraction'] = '%s \'%s\' ezabatu';
$string['deleteinteractionsure'] = 'Ziur al zaude hau egin nahi duzula? Ezingo da berreskuratu.';
$string['deletespecifiedgroup'] = 'Ezabatu \'%s\' taldea';
$string['denyfriendrequest'] = 'Ezetsi Laguntasun-eskaera';
$string['denyfriendrequestlower'] = 'Ezetsi laguntasun-eskaera';
$string['denyrequest'] = 'Ezetsi eskaera';
$string['editgroup'] = 'Editatu taldea';
$string['editgroupmembership'] = 'Editatu taldekideak';
$string['editmembershipforuser'] = 'Editatu taldekideak %s(r)entzat';
$string['existingfriend'] = 'dagoeneko laguna';
$string['findnewfriends'] = 'Bilatu lagun berriak';
$string['friend'] = 'lagun';
$string['friendformacceptsuccess'] = 'Laguntasun-eskaera onartu da';
$string['friendformaddsuccess'] = '%s zure lagun-zerrendara gehitu da';
$string['friendformrejectsuccess'] = 'Laguntasun-eskaera ezetsi da';
$string['friendformremovesuccess'] = '%s zure lagun-zerrendatik ezabatua izan da';
$string['friendformrequestsuccess'] = 'Bidali laguntasun-eskaera %s-(r)i';
$string['friendlistfailure'] = 'Ezin izan da zure lagunen zerrenda eguneratu';
$string['friendrequestacceptedmessage'] = '%s(e)k zure laguntasun-eskaera onartu du eta zure lagun zerrendara gehituko da';
$string['friendrequestacceptedsubject'] = 'Laguntasun-eskaera onartu da';
$string['friendrequestrejectedmessage'] = '%s-(e)k ezezkoa eman dio zure laguntasun-eskaerari ezezkoa.';
$string['friendrequestrejectedmessagereason'] = '%s-(e)k zure laguntasun-eskaera ezetsi du.  Arrazoiak ondokoak dira:';
$string['friendrequestrejectedsubject'] = 'Laguntasun-eskaera ezetsi da';
$string['friends'] = 'lagun';
$string['friendshipalreadyrequested'] = '%s(r)en lagun-zerrendan sartzeko eskaera egin duzu';
$string['friendshipalreadyrequestedowner'] = '%s(e)k zure lagun-zerrendan sartzeko eskaera egin du';
$string['friendshiprequested'] = 'Laguntasun-eskaera egin da!';
$string['group'] = 'taldea';
$string['groupadmins'] = 'Talde-kudeatzaileak';
$string['groupalreadyexists'] = 'Badago izen hori duen talde bat';
$string['groupcategory'] = 'Talde-kategoria';
$string['groupconfirmdelete'] = 'Ziur al zaude talde hau ezabatu nahi duzula?';
$string['groupconfirmdeletehasviews'] = 'Ziur al zaude talde hau ezabatu nahi duzula? Zure zenbait bistek talde hau erabiltzen dute sarrera-kontrol gisa; talde hau ezabatzen baduzu, partaideek ezingo dute bista horietara sartu.';
$string['groupconfirmleave'] = 'Ziur al zaude talde hau utzi nahi duzula?';
$string['groupconfirmleavehasviews'] = 'Ziur al zaude talde hau utzi nahi duzula? Zure zenbait bistek talde hau erabiltzen dute sarrera-kontrol gisa, talde hau uzten baduzu, partaideek ezingo dute bista horietara sartu.';
$string['groupdescription'] = 'Taldearen deskribapena';
$string['grouphaveinvite'] = 'Talde honetan sartzera gonbidatu zaituzte';
$string['grouphaveinvitewithrole'] = 'Telde honetan sartzera gonbidatu zaituzte honako rolarekin';
$string['groupinteractions'] = 'Talde-jarduerak';
$string['groupinviteaccepted'] = 'Gonbidapena ondo onartu da! Taldeko partaidea zara orain';
$string['groupinvitedeclined'] = 'Gonbidapena ondo ezetsi da!';
$string['groupinvitesfrom'] = 'Gonbidatu elkartzera:';
$string['groupjointypecontrolled'] = 'Talde honetako partaidetza kontrolatuta dago. Ezin zara talde honetan sartu.';
$string['groupjointypeinvite'] = 'Talde honetako partaidetza gonbidapen bidezkoa da soilik.';
$string['groupjointypeopen'] = 'Talde honetako partaidetza irekia da. Sartu lasai!';
$string['groupjointyperequest'] = 'Talde honetako partaidetza eskaera bidezkoa da soilik.';
$string['groupmemberrequests'] = 'Onartzeke dauden partaidetza-eskaerak';
$string['groupmembershipchangedmessageaddedmember'] = 'Partaide gisa gehitu zaituzte talde honetan';
$string['groupmembershipchangedmessageaddedtutor'] = 'Tutore gisa gehitu zaituzte talde honetan';
$string['groupmembershipchangedmessagedeclinerequest'] = 'Talde honetan sartzeko eskaera ezetsi egin da';
$string['groupmembershipchangedmessagemember'] = 'Talde honen tutore izateari utzi diozu';
$string['groupmembershipchangedmessageremove'] = 'Talde honetatik ezabatu zaituzte';
$string['groupmembershipchangedmessagetutor'] = 'Tutore gisa jarri zaituzte talde honetan';
$string['groupmembershipchangesubject'] = 'Taldeko partaidetza: %s';
$string['groupname'] = 'Taldearen izena';
$string['groupnotfound'] = 'Ez da aurkitu %s id-dun talderik';
$string['groupnotinvited'] = 'Ez zaituzte talde honetan sartzera gonbidatu';
$string['groupoptionsset'] = 'Taldearen aukerak eguneratu dira.';
$string['grouprequestmessage'] = '%s(e)k zure %s taldean sartu nahiko luke';
$string['grouprequestmessagereason'] = '%s(e)k zure %s taldean sartu nahiko luke. Sartu nahi izateko arrazoia ondokoa da:

%s';
$string['grouprequestsent'] = 'Taldeko partaide izateko eskaera bidali da';
$string['grouprequestsubject'] = 'Taldeko partaide izateko eskaera berria';
$string['groups'] = 'talde';
$string['groupsaved'] = 'Taldea ondo gorde da';
$string['groupsimin'] = 'Talde hauetako partaide naiz';
$string['groupsiminvitedto'] = 'Talde hauek gonbidatu naute';
$string['groupsiown'] = 'Talde hauen jabe naiz';
$string['groupsiwanttojoin'] = 'Talde hauetan sartu nahi  dut';
$string['groupsnotin'] = 'Ez naiz talde hauetako partaide';
$string['grouptype'] = 'Talde-mota';
$string['hasbeeninvitedtojoin'] = 'talde honi atxikitzera gonbidatu da';
$string['hasrequestedmembership'] = 'talde honetan parte hartzeko eskaera egin du';
$string['interactiondeleted'] = '%s ondo ezabatu da';
$string['interactionsaved'] = '%s ondo gorde da';
$string['invalidgroup'] = 'Taldea ez da existitzen';
$string['invitationssent'] = '%d gonbideapen bidalita';
$string['invite'] = 'Gonbidatu';
$string['invitemembertogroup'] = '%s \'%s\'(e)ra sartzera gonbidatu';
$string['invites'] = 'Gonbidapenak';
$string['invitetogroupmessage'] = '%s(e)k talde batean sartzera gonbidatu zaitu, \'%s\'.  Klikatu jarraian dagoen estekan informazio gehiago lortzeko.';
$string['invitetogroupsubject'] = 'Talde batetan sartzera gonbidatu zaituzte';
$string['inviteuserfailed'] = 'Ezin izan da erabiltzailea gonbidatu';
$string['inviteusertojoingroup'] = 'Gonbidatu';
$string['joinedgroup'] = 'Taldeko partaidea zara orain';
$string['joingroup'] = 'Izan talde honetako partaide';
$string['leavegroup'] = 'Talde hau utzi';
$string['leavespecifiedgroup'] = '\'%s\' taldea utzi';
$string['leftgroup'] = 'Talde hau utzi duzu orain';
$string['leftgroupfailed'] = 'Taldea uzteko ahaleginak huts egin du';
$string['member'] = 'partaide';
$string['memberchangefailed'] = 'Ezin izan da eguneratu partaidetzari buruzko informazioa';
$string['memberchangesuccess'] = 'Partaidetza-estatusa ondo aldatu da';
$string['memberrequests'] = 'Partaidetza-eskaerak';
$string['members'] = 'partaide';
$string['membersdescription:controlled'] = 'Partaidetza kontrolatuko taldea da hau. Beren profil-orritik gehi ditzakezu erabiltzaileak edo <a href="%s">hainbat erabiltzaile batera gehitu</a>';
$string['membersdescription:invite'] = 'Gonbidatutakoentzako taldea da hau. Beren profil-orritik gehi ditzakezu erabiltzaileak edo <a href="%s">hainbat erabiltzaile batera gehitu</a>';
$string['membershiprequests'] = 'Taldekide izateko eskaerak';
$string['membershiptype'] = 'Taldeko partaidetza mota';
$string['membershiptype.abbrev.controlled'] = 'Kontrolatua';
$string['membershiptype.abbrev.invite'] = 'Gonbidatu';
$string['membershiptype.abbrev.open'] = 'Zabaldu';
$string['membershiptype.abbrev.request'] = 'Eskaria';
$string['membershiptype.controlled'] = 'Kontrolatutako partaidetza';
$string['membershiptype.invite'] = 'Gonbidatuak bakarrik';
$string['membershiptype.open'] = 'Partaidetza irekia';
$string['membershiptype.request'] = 'Eskatu partaidetza';
$string['memberslist'] = 'Partaideak:';
$string['messagebody'] = 'Bidali mezua';
$string['messagenotsent'] = 'Ezin izan da mezua bidali';
$string['messagesent'] = 'Mezua bidali da!';
$string['moregroups'] = 'Talde gehiago';
$string['newmembersadded'] = 'Gehitutako %d partaide berriak';
$string['newusermessage'] = '%s(r)en mezu berria';
$string['newusermessageemailbody'] = '%s(e)k mezua bidali dizu. Mezua ikusteko bisitatu %s';
$string['nobodyawaitsfriendapproval'] = 'Ez dago inor zuk lagun gisa onartzeko zain';
$string['nocategoryselected'] = 'Ez da kategoria aukeratu';
$string['nogroups'] = 'Ez dago talderik';
$string['nogroupsfound'] = 'Ez da talderik aurkitu';
$string['nointeractions'] = 'Talde honetan ez dago jarduerarik';
$string['nosearchresultsfound'] = 'Bilaketak ez du emaitzarik izan :(';
$string['notallowedtodeleteinteractions'] = 'Ezin dituzu talde honetako jarduerak ezabatu';
$string['notallowedtoeditinteractions'] = 'Talde honetan ezin da jarduerarik gehitu edo editatu';
$string['notamember'] = 'Ez zara talde honetako partaidea';
$string['notinanygroups'] = 'Ez dago inongo taldetan';
$string['notmembermayjoin'] = '\'%s\' taldera atxiki behar zara orrialde hau ikusteko.';
$string['notpublic'] = 'Talde hau ez da publikoa.';
$string['noviewstosee'] = 'Zuk ikus dezakezunik ez :(';
$string['pending'] = 'Onartzeke';
$string['pendingfriends'] = 'Onartzeke dauden lagunak';
$string['pendingmembers'] = 'Onartzeke dauden partaideak';
$string['potentialmembers'] = 'Balizko partaideak';
$string['publiclyviewablegroup'] = 'Publikoki ikusteko moduko taldea?';
$string['publiclyviewablegroupdescription'] = 'Onartu edonork ikustea talde hau eta bertako foroak (guneko kide ez direnek barne)?';
$string['publiclyvisible'] = 'Publikoa';
$string['reason'] = 'Arrazoia';
$string['reasonoptional'] = 'Arrazoia (aukerakoa)';
$string['reject'] = 'Ezetsi';
$string['rejectfriendshipreason'] = 'Eskaerari ezezkoa emateko arrazoiak';
$string['releaseview'] = 'Askatu orria';
$string['remove'] = 'Ezabatu';
$string['removedfromfriendslistmessage'] = '%s-(e)k lagun-zerrendatik ezabatu zaitu.';
$string['removedfromfriendslistmessagereason'] = '%s-(e)k lagun-zerrendatik ezabatu zaitu. Emandako arrazoia ondokoa da:';
$string['removedfromfriendslistsubject'] = 'Lagun-zerrendatik ezabatuta';
$string['removedfromgroupsmessage'] = '%s-k kendu egin zaitu talde honetatik/hauetatik: %s';
$string['removefriend'] = 'Laguna ezabatu';
$string['removefromfriends'] = '%s ezabatu lagun-zerrendatik';
$string['removefromfriendslist'] = 'Lagun-zerrendatik ezabatu';
$string['removefromgroup'] = 'Ezabatu taldetik';
$string['request'] = 'Eskaera';
$string['requestedfriendlistmessage'] = '%s(e)k lagun gisa gehitzeko eskatzen dizu. Jarraian duzun esteka erabilita egin dezakezu, edo lagun-zerrendaren orrialdetik.';
$string['requestedfriendlistmessagereason'] = '%s-(e)k lagun gisa gehitzeko eskatzen dizute. Jarraian duzun esteka erabililta egin dezakezu, edo lagun-zerrendaren orrialdetik. Emandako arrazoia ondokoa da:';
$string['requestedfriendlistsubject'] = 'Laguntasun-eskaera berria';
$string['requestedfriendship'] = 'laguntasun-eskaera egin da';
$string['requestedmembershipin'] = 'Partaidetza-eskaera egin da hemen:';
$string['requestedtojoin'] = 'Talde honetan sartzea eskatu duzu';
$string['requestfriendship'] = 'Laguntasun-eskaera egin';
$string['requestjoingroup'] = 'Talde honetan sartzeko eskaera egin';
$string['requestjoinspecifiedgroup'] = '\'%s\' taldean sartzeko eskaera egin';
$string['requests'] = 'Eskaerak';
$string['rolechanged'] = 'Rola aldatuta';
$string['savegroup'] = 'Gorde taldea';
$string['seeallviews'] = '%s orri guztiak ikusi...';
$string['sendfriendrequest'] = 'Bidali laguntasun-eskaera!';
$string['sendfriendshiprequest'] = 'Bidali laguntasun-eskaera %s(r)i';
$string['sendinvitation'] = 'Bidali gonbidapena';
$string['sendinvitations'] = 'Bidali gonbidapenak';
$string['sendmessage'] = 'Bidali mezua';
$string['sendmessageto'] = 'Bidali mezua %s(r)i';
$string['submit'] = 'Bidali';
$string['submittedviews'] = 'Bidalitako orriak';
$string['therearependingrequests'] = '%s partaidetza-eskaera daude onartzeke talde honetan';
$string['thereispendingrequest'] = 'Partaidetza-eskaera bat dago onartzeke talde honetan';
$string['title'] = 'Izenburua';
$string['trysearchingforfriends'] = 'Erabili %sbilatu lagun berriak%s zure sarea zabaltzeko!';
$string['trysearchingforgroups'] = 'Erabili %sbilatu taldeak%s!';
$string['updatemembership'] = 'Eguneratu partaidetza';
$string['user'] = 'erabiltzaile';
$string['useradded'] = 'Erabiltzailea gehitu da';
$string['useralreadyinvitedtogroup'] = 'Erabiltzaile hau gonbidatu izan da talde honetara, edo bertako partaide da.';
$string['usercannotchangetothisrole'] = 'Erabiltzailea ezin da aldatu rol honetara.';
$string['usercantleavegroup'] = 'Erabiltzaileak ezin du talde hau utzi.';
$string['userdoesntwantfriends'] = 'Erabiltzaile honek ez du lagun berririk nahi';
$string['userinvited'] = 'Gonbidapena ondo bidali da';
$string['userremoved'] = 'Erabiltzailea ezabatuta';
$string['users'] = 'erabiltzaile';
$string['usersautoadded'] = 'Erabiltzaileek izena eman dezakete?';
$string['usersautoaddeddescription'] = 'Erabiltzaile berri guztiak talde honetan automatikoki gehituko dira?';
$string['userstobeadded'] = 'Gehitzeko erabiltzaileak';
$string['userstobeinvited'] = 'Gonbidatzeko erabiltzaileak';
$string['viewmessage'] = 'Ikusi mezua';
$string['viewnotify'] = 'Ikusi jakinarazpenak';
$string['viewnotifydescription'] = 'Markatuta, taldeko partaideei jakinarazpen bana bidaliko zaie partaideren batek taldearentzat Orri bat sareratzen duen bakoitzean. Ezarpen hau talde handietan gaituta jakinarazpen piloa sor daiteke.';
$string['viewreleasedmessage'] = '%s(e)k itzuli egin dizu %s taldeari bidali zenion orria';
$string['viewreleasedsubject'] = 'Zure orria askatu dute';
$string['viewreleasedsuccess'] = 'Orria ondo askatu da';
$string['whymakemeyourfriend'] = 'Zure lagun gisa onartzeko arrazoiak:';
$string['youaregroupadmin'] = 'Talde honetako kudeatzailea zara';
$string['youaregroupmember'] = 'Talde honetako partaidea zara';
$string['youaregrouptutor'] = 'Talde honetako tutoreetako bat zara';
$string['youowngroup'] = 'Talde honen jabea zara';
?>
