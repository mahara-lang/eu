<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['aboutdefaultcontent'] = '<h2>Maharari buruz</h2>

<p>2006an sortu zen, Mahara Zelanda Berriko Helduentzako Hezkuntza Komisioko e-Learning Collaborative Development Fund-en(eCDF) ekimenez eta honako erakundeekiko lankidetzan: Masseyko Unibertsitatea, Auckland-eko Teknologia Unibertsitatea, Zelanda Berriko Politekniko Irekia eta Wellingtongo Victoria Unibertsitatea.</p>

<p>Mahara portfolio elektroniko, egunkari, aurkezpen-eraikitzaile eta sare sozialen sistema bat da, erabiltzaileak konektatu eta online-komunitateak sortzeko. Maharak tresnak eskaintzen dizkie erabiltzaileei ikasketa-  eta garapen-ingurune pertsonal zein profesionala sortzeko.</p>

<p>Te Reo Maori hizkuntzan `pentsatu\' or `pentsatutakoa\' esan nahi du. Izen horrek proiektu honetan aritu direnen lana islatzen du, erabiltzaileengan zentraturiko bizitza osorako ikasketa eta garapenerako proiektua sortzeko. Horrez gain, uste sendo bat ere islatzen du: hain zuzen ere, teknologiak eskaintzen dituen irtenbideak ezin direla garatu pedagogia eta politika-irizpideetatik aparte.</p>

<p>Mahara doan eskaintzen da Software Aske gisa (GNU Lizentzia Publiko Orokorpean). Labur esateko: Mahara kopiatu, erabili eta eraldatu egin dezakezu ondokoarekin ados bazaude:</p>

<ul>
    <li>kode-iturria besteen eskura uztea; </li>
    <li>ez aldatu edo ezabatzea jatorrizko lizentzia eta copyrighta, eta</li>
    <li>lizentzia bera ezartzea egindako lan eratorri guztiei.</li>
</ul>

<p>Mesedez <a href="contact.php">jarri harremanetan gurekin</a> Mahararen inguruko galderarik baduzu.</p>

<p><a href="http://mahara.org">http://mahara.org</a></p>';
$string['homedefaultcontent'] = '<h2>Ongi etorri Maharara</h2>

<p>Mahara portfolio elektroniko, egunkari, aurkezpen-eraikitzaile eta sare sozialen sistema bat da, erabiltzaileak konektatu eta online-eko komunitateak sortzeko. Maharak tresnak eskaintzen dizkie erabiltzaileei ikasketa-  eta garapen-ingurune pertsonal zein profesionala sortzeko.</p>

<p>Mahararen inguruan informazio gehiago lortzeko <a href="about.php">Honi buruz</a> irakur dezakezu edo <a href="contact.php">jarri harremanean gurekin</a>.</p>';
$string['loggedouthomedefaultcontent'] = '<h2>Ongi etorri Maharara</h2>

<p>Mahara portfolio elektroniko, egunkari, aurkezpen-eraikitzaile eta sare sozialen sistema bat da, erabiltzaileak konektatu eta online-eko komunitateak sortzeko. Maharak tresnak eskaintzen dizkie erabiltzaileei ikasketa-  eta garapen-ingurune pertsonal zein profesionala sortzeko.</p>

<p>Mahararen inguruan informazio gehiago lortzeko <a href="about.php">Honi buruz</a> irakur dezakezu edo  <a href="contact.php">jarri harremanean gurekin</a>.</p>';
$string['privacydefaultcontent'] = '<h2>Pribatutasun-kontratua</h2>
    
<h3>Aurkezpena</h3>

<p>Konpromisoa hartu dugu zure pribatutasuna babestu eta erabiltzaileei ikasketa eta garapenerako ingurune funtzional seguru bat eskaintzeko. Pribatutasun-kontratu hau Mahara gunean eta gordetako datu bildumetan aplikatzen da.</p>

<h3>Informazio pertsonalaren bilketa</h3>

<p>Maharan erregistratu ahal izateko zenbait datu pertsonal eskatzen dizkizugu. Informazio pertsonal hori ez dugu beste pertsona edota erakunderen baten eskura utziko zuk idatzizko baimena eman ezean edo eskakizun judizialagatik ez bada.</p>

<h3>Cookiak</h3>

<p>Mahara erabiltzeko cookiak gaituta izan behar dituzu nabigatzailean. Azpimarratu nahi dugu Maharan erabiltzen diren cookietan ez dela informazio pribaturik gordetzen.</p>

<p>Cookiea web-orri zerbitzari batek zure ordenagailuan jartzen duen datu-fitxategi bat da. Cookieak ez dira programak, spywareak edo birusak eta ezin dute eragiketarik egin beren kabuz.</p>

<h3>Zure informazioa nola erabiltzen dugun</h3>

<p>Zure informazio pertsonala, zuk jarritako baldintzen baitan erabiliko dugu.</p>

<p>Mahararen erabiltzaile zaren heinean, zeuk erabakitzen duzu zein informazio pertsonal jarri besteen eskura. Lehenetsia dago, sarrera-eskubidea eman zaien Administratzaile, Ikastaro Sortzaile edo Tutoreek salbu, erabiltzaileek zure goitizena baino ez dutela ikusiko. Horrek Ohartarazpen-txostenak eta Bisitari Sarrera ere hartzen ditu barne.</p>

<p>Sistemaren erabilpenari buruzko estatistikak sortzeko ere datuak hartuko ditugu, baina bertan ez da erabiltzaile indibidualik identifikatuko./p>

<h3>Informazio pertsonalaren bilketa eta segurtasuna</h3>

<p>Zentzuzko neurri guztiak hartuko ditugu guregana heltzen den informazio pertsonala ez dadin galdu, nahasi, edo baimenik gabe ireki edo eraldatu.</p>

<p>Zure informazio pertsonala babesteko, mesedez ez eman zure Erabiltzaile-izena edo Pasahitza Gunearen Administratzailea ez den beste inori.</p>

<h3>Aldaketak pribazitate-kontratu honetan</h3>

<p>Inoiz aldaketak egin ditzakegu pribazitate kontratu honetan sisteman gertatutako aldaketak jaso ahal izateko edo bezeroen eskakizunei erantzuteko; horrenbestez, gunean sartzen zaren bakoitzean Pribazitate Kontratua begiratzea gomendatzen dizugu.</p>

<h3>Kontaktua</h3>

<p>Kontratu honen inguruan galderaren bat baldin baduzu edo zehaztutako baldintzak bete ez ditugula uste baduzu, mesedez <a href="contact.php">jarri harremanean gurekin</a> eta arazoa konpontzen ahaleginduko gara.</p>';
$string['termsandconditionsdefaultcontent'] = '<h2>Arau eta Baldintzak</h2>

<p>[Mahara] erabiltzeko ondoko Arau eta Baldintzekin ados egon behar duzu.</p>

<h3>Gure Betebeharrak</h3>

<p>[Mahararen] Gune-administratzaileek konpromisoa hartzen dute portfolio-sistema seguru eta operatiboa eskaintzeko erabiltzaile guztiei, eta horretarako, behar diren zentzuzko baliabide guztiak jarriko dituzte. Zure erabiltzaile eskubideak urratu direla uste baduzu edo galderaren bat baduzu honen inguruan, mesedez <a href="contact.php">jarri harremanean gurekin</a> lehenbailehen.</p>

<p>[Mahara] inoiz ezgaituta egon daiteke epe labur batez sistema eguneratzeko. Gutxienez 3 laneguneko aurrerapenarekin emango da mozketaren berri.</p>

<p>Material edo jarrera desegokiak aurkituko bazenitu, jakinarazi <a href="contact.php">Gunearen Administratzaileari</a> lehenbailehen. Ahal bezain laster ikertuko da arazoa.</p>

<p>Gunearen Administratzailea zure portfolio eta edukietara sar daiteke edozein unetan; hala ere, ez du ezer egingo zure erabiltzaile zerbitzuari laguntzeko ez bada edo Arau eta Baldintzetatik kanpo badago.</p>

<h3>Zure betebeharrak</h3>

<p><a href="privacy.php">Pribatutasun-kontratua</a> Arau eta Baldintzen luzapen gisa ulertu behar da eta erabiltzaile guztiek irakurri behar dute.</p>

<p>Zure [Mahara] kontua  iraungi egingo da eman zaion epe edo aktibitaterik gabeko denbora igarotakoan, Gunearen Administratzaileak ezarritakoaren arabera. Zure kontua iraungi baino lehen, gogorarazpen-emaila jasoko duzu eta portfolioan duzuna ordenagailu pertsonalean gordetzea gomendatzen dizugu, etorkizunean erabili ahal izateko.</p>

<p>[Maharara] igotzen dituzun fitxategi eta eduki guztiak Euskal Herriko Copyright- legeen menpe egongo dira. Zeure ardura da egiaztatzea beharrezko baimenak dituzula zurea ez den edozein lan argitaratzeko. Plagio-arazoak zure ikastetxearen politiken baitan konponduko dira.</p>

<p>Ez duzu zure portfolioa erabiliko material iraingarria gordetzeko edo erakusteko. Gunearen Administratzaileak oharrik jasotzen badu zure portfolioan material desegokia dagoela esanez, zure kontua eten egingo da eta [Maharan] sartzeko aukera ukatuko zaizu zure Ikasleen Jarrera Politikaren arduradunek ikertu bitartean. Jarrera-politikarik ez bada, erakunde edo elkartean gai hauetaz arduratzen denari helaraziko zaio.</p>

<p>Administratzaileak [Maharan] izan duzun jarrerari buruzko kexaren bat jasotzen baldin badu, zure kontua eten egingo da eta [Maharan] sartzeko aukera ukatuko zaizu zure Ikasleen Jarrera Politikaren arduradunek ikertu bitartean. Jarrera-politikarik ez bada, erakunde edo elkartean gai hauetaz arduratzen denari helaraziko zaio.</p>

<p>Jarrera desegoki gisa ulertuko da, orobat, material desegokien ohartarazte-sistema arrazoirik gabe erabiltzea, birusak dauzkaten fitxategiak nahita igotzen saiatzea, feedback edo komentario desegoki edo gehiegizkoak egitea beste edozein erabiltzaileren portfolioan eta Gunearen Administratzaileak iraingarri edo kaltegarritzat jo dezakeen beste edozein jarrera.</p>

<p>Zeure ardurapean geratzen dira jaso ditzakezun kontaktuak publiko egin duzun informazio pertsonala dela medio; hala ere, sistemako erabiltzaileek izan dezaketen jarrera okerrak <a href="contact.php">Gunearen Administratzaileari</a> jakinarazi beharko litzaioke lehenbailehen. Gerta daiteke noizean behin aldaketa txikiak egitea gure Arau eta Baldintzetan erabiltzaileen eskaerei erantzuteko. Gunean sartzen zaren bakoitzean Arau eta Baldintzak begiratzea gomendatzen dizugu. Hala ere, Arau eta Baldintzetan aldaketa garrantzitsurik egiten bada, [Mahararen] orrialde nagusiaren bitartez jakinaraziko da.</p>';
$string['uploadcopyrightdefaultcontent'] = 'Bai: igotzen ari naizen fitxategia nirea da edo baimen zehatza dut item hau erreproduzitu eta/edo zabaltzeko. Fitxategi hau erabilita ez dut nire herrialdeko Copyright-legea. Fitxategi hau, era berean, bat dator gune honetako Arau eta Baldintzekin.';
?>
