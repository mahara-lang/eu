<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['element.bytes.bytes'] = 'Byteak';
$string['element.bytes.gigabytes'] = 'Gigabyteak';
$string['element.bytes.invalidvalue'] = 'Baloreak zenbakia izan behar du';
$string['element.bytes.kilobytes'] = 'Kilobyteak';
$string['element.bytes.megabytes'] = 'Megabyteak';
$string['element.calendar.invalidvalue'] = 'Data/ordua desegokia';
$string['element.date.monthnames'] = 'Urtarrila,Otsaila,Martxoa,Apirila,Maiatza,Ekaina,Uztaila,Abuztua,Iraila,Urria,Azaroa,Abendua';
$string['element.date.notspecified'] = 'Zehaztu gabea';
$string['element.date.or'] = 'edo';
$string['element.expiry.days'] = 'Egunak';
$string['element.expiry.months'] = 'Hilabeteak';
$string['element.expiry.noenddate'] = 'Amaiera-datarik ez';
$string['element.expiry.weeks'] = 'Asteak';
$string['element.expiry.years'] = 'Urteak';
$string['rule.before.before'] = 'Hori ezin da "%s" eremuaren atzean egon.';
$string['rule.email.email'] = 'E-posta helbideak ez du balio';
$string['rule.integer.integer'] = 'Eremu honek integer behar luke izan.';
$string['rule.maxlength.maxlength'] = 'Eremu honek gutxienez %d karaktereko luzaera izan behar du';
$string['rule.maxvalue.maxvalue'] = 'Balore hau ezin da %d baino luzeagoa izan';
$string['rule.minlength.minlength'] = 'Eremu honek gutxienez %d karaktereko luzaera izan behar du';
$string['rule.minvalue.minvalue'] = 'Balore hau ezin da izan %d baino txikiagoa';
$string['rule.regex.regex'] = 'Eremu honen formatoa ez da egokia';
$string['rule.required.required'] = 'Eremu hau nahitaezkoa da';
$string['rule.validateoptions.validateoptions'] = '"%s" aukerak ez du balio';
?>
