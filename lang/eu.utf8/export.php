<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - Axular Lizeoa (www.axular.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['Done'] = 'Eginda';
$string['Export'] = 'Esportatu';
$string['Starting'] = 'Hasi da';
$string['allmydata'] = 'Nire datu guztiak';
$string['chooseanexportformat'] = 'Aukera ezazu esportatzeko formatu bat';
$string['clicktopreview'] = 'Klik egin aurreikusteko';
$string['collectionstoexport'] = 'Esportatzeko bildumak';
$string['creatingzipfile'] = 'Zip fitxategia sortzen';
$string['exportgeneratedsuccessfully'] = 'Esportatzeko fitxategia ondo sortu da. %sKlik  egin hemen jaisteko%s';
$string['exportgeneratedsuccessfullyjs'] = 'Esportatzeko fitxategia ondo sortu da. %sJarraitu%s';
$string['exportingartefactplugindata'] = 'Tresna-pluginen datuak esportatzen';
$string['exportingartefacts'] = 'Tresnak esportatzen';
$string['exportingartefactsprogress'] = 'Tresnak esportatzen: %s/%s';
$string['exportingfooter'] = 'Orripekoa esportatzen';
$string['exportingviews'] = 'Orriak esportatzen';
$string['exportingviewsprogress'] = 'Orriak esportatzen: %s/%s';
$string['exportpagedescription'] = 'Tresna honek zure portfolioaren informazioa eta orriak esportatzen ditu, baina ezin du gunearen ezarpenik esportatu.';
$string['exportyourportfolio'] = 'Esportatu zure portfolioa';
$string['generateexport'] = 'Sortu esportazioa';
$string['justsomecollections'] = 'Nire bildumetako batzuk';
$string['justsomeviews'] = 'Nire orrietako batzuk';
$string['noexportpluginsenabled'] = 'Kudeatzaileak desgaitu egin du pluginak ez esportatzea, beraz ezin duzu erabili funtzio hau';
$string['nonexistentfile'] = 'Ez zegoen fitxategia igotzen saiatu zara: \'%s\'';
$string['pleasewaitwhileyourexportisbeinggenerated'] = 'Mesedez, itxaron pixka batean esportatzeko fitxategia sortu arte...';
$string['reverseselection'] = 'Desegin aukeraketa';
$string['selectall'] = 'Aukeratu guztia';
$string['setupcomplete'] = 'Instalazioa eginda';
$string['unabletoexportportfoliousingoptions'] = 'Ezin da portfolioa esportatu egindako aukerak erabiliz';
$string['unabletogenerateexport'] = 'Ezin da esportazioa sortu';
$string['viewstoexport'] = 'Esportatzeko orriak';
$string['whatdoyouwanttoexport'] = 'Zer esportatu nahi duzu?';
$string['writingfiles'] = 'Fitxategiak idazten';
$string['youarehere'] = 'Hemen zaude';
$string['youmustselectatleastonecollectiontoexport'] = 'Gutxienez bilduma bat aukeratu behar duzu esportatzeko';
$string['youmustselectatleastoneviewtoexport'] = 'Gutxienez orri bat aukeratu behar duzu esportatzeko';
$string['zipnotinstalled'] = 'Zure sistemak ez du zip komandoa. Mesedez, instalatu zip-a funtzio hau gaitzeko.';
?>
