<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['Created'] = 'Sortua';
$string['Description'] = 'Deskribapena';
$string['Download'] = 'Deskargatu';
$string['Owner'] = 'Jabea';
$string['Preview'] = 'Aurreikusi';
$string['Size'] = 'Neurria';
$string['Title'] = 'Izena';
$string['Type'] = 'Mota';
$string['aboutdescription'] = 'Jarri hemen zure benetako izen-deiturak. Beste izen bat erakutsi nahi baduzu sisteman, jarri izen hori zure lehenetsitako izen gisa.';
$string['aboutme'] = 'Niri buruz';
$string['addbutton'] = 'Gehitu';
$string['address'] = 'Posta-helbidea';
$string['aimscreenname'] = 'AIM pantailaren izena';
$string['blogaddress'] = 'Egunkariaren helbidea';
$string['businessnumber'] = 'Laneko telefonoa';
$string['city'] = 'Lurraldea';
$string['contact'] = 'Kontakturako informazioa';
$string['contactdescription'] = 'Informazio hau guztia pribatua da, bista batean jartzea aukeratzen ez duzun artean.';
$string['country'] = 'Herrialdea';
$string['editmyprofile'] = 'Editatu nire profila';
$string['editprofile'] = 'Editatu profila';
$string['email'] = 'E-posta helbidea (bat baino gehiago onartzen da)';
$string['emailactivation'] = 'E-postaren egiaztatzea';
$string['emailactivationdeclined'] = 'E-postaren egiaztatzea bertan behera geratu da';
$string['emailactivationfailed'] = 'Ezin izan da e-posta egiaztatu';
$string['emailactivationsucceeded'] = 'E-posta ondo egiaztatu da';
$string['emailaddress'] = 'Ordezko e-posta';
$string['emailalreadyactivated'] = 'e-posta dagoeneko aktibatuta dago';
$string['emailingfailed'] = 'Profila gorde da, baina ez da emailik bidali ondoko helbidera: %s';
$string['emailvalidation_body'] = 'Kaixo %s,

%s e-posta zure Mahara-ko erabiltzaile-kontuan gehitu da. Mesedez, klikatu jarraian dagoen lotura helbidea aktibatzeko.

%s';
$string['emailvalidation_subject'] = 'E-posta egiaztatu';
$string['faxnumber'] = 'Fax zenbakia';
$string['firstname'] = 'Izena';
$string['fullname'] = 'Izen osoa';
$string['general'] = 'Orokorra';
$string['homenumber'] = 'Etxeko-telefonoa';
$string['icqnumber'] = 'ICQ zenbakia';
$string['industry'] = 'Lantokia';
$string['infoisprivate'] = 'Informazio hau pribatua da beste batzuek ikus dezaketen orri batean gehitu arte.';
$string['institution'] = 'Erakundea';
$string['introduction'] = 'Aurkezpena';
$string['invalidemailaddress'] = 'E-posta helbideak ez du balio';
$string['jabberusername'] = 'Jabber-eko erabiltzaile-izena';
$string['lastmodified'] = 'Azken aldaketa';
$string['lastname'] = 'Deitura';
$string['loseyourchanges'] = 'Aldaketak galdu al dituzu?';
$string['maildisabled'] = 'E-posta desgaituta';
$string['mandatory'] = 'Beharrezkoa';
$string['messaging'] = 'Mezuak';
$string['messagingdescription'] = 'Kontakturako informazioa bezala, hau ere pribatua da.';
$string['mobilenumber'] = 'Eskuko telefonoa';
$string['msnnumber'] = 'MSN txata';
$string['myfiles'] = 'Nire fitxategiak';
$string['name'] = 'Izena';
$string['occupation'] = 'Lanbidea';
$string['officialwebsite'] = 'Webgune ofizialaren helbidea';
$string['personalwebsite'] = 'Webgune pertsonalaren helbidea';
$string['pluginname'] = 'Profila';
$string['preferredname'] = 'Hobetsitako izena';
$string['principalemailaddress'] = 'Email nagusia';
$string['profile'] = 'Profila';
$string['profilefailedsaved'] = 'Ezin izan da profila gorde';
$string['profileinformation'] = 'Profilaren informazioa';
$string['profilepage'] = 'Profilaren orria';
$string['profilesaved'] = 'Profila ondo gorde da';
$string['public'] = 'Publikoa';
$string['saveprofile'] = 'Gorde profila';
$string['skypeusername'] = 'Skype-ko erabiltzaile-izena';
$string['studentid'] = 'Ikaslearen IDa';
$string['town'] = 'Hiria';
$string['unvalidatedemailalreadytaken'] = 'Erabili nahi duzun email-helbidea hartuta dago.';
$string['validationemailsent'] = 'egiaztapenerako e-posta bidali da';
$string['validationemailwillbesent'] = 'Profila gordetzean egiaztatzeko e-posta bidaliko zaizu';
$string['verificationlinkexpired'] = 'Egiaztapenerako esteka zaharkitu egin da';
$string['viewallprofileinformation'] = 'Ikusi profilaren informazio osoa';
$string['viewmyprofile'] = 'Ikusi nire profila';
$string['viewprofilepage'] = 'Ikusi profil-orria';
$string['yahoochat'] = 'Yahoo txata';
?>
