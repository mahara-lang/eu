<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['aboutme'] = 'Niri buruz';
$string['description'] = 'Aukeratu erakutsiko den profil-informazioa';
$string['dontshowemail'] = 'Ez erakutsi e-posta helbidea';
$string['dontshowprofileicon'] = 'Ez erakutsi profil-ikonoa';
$string['fieldstoshow'] = 'Erakutsiko diren eremuak';
$string['introtext'] = 'Aurkezpen-testua';
$string['title'] = 'Profil-informazioa';
$string['uploadaprofileicon'] = 'Ez duzu profil-ikonorik. <a href="%sartefact/file/profileicons.php" target="_blank">Igo bat</a>';
$string['useintroductioninstead'] = 'Aurkezpen-profila gaituz gero, hura erabil dezakezu eta zuriz utzi eremu hau';
?>
