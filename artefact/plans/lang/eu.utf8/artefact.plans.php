<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['Plans'] = 'Egitasmoak';
$string['alltasks'] = 'Lan guztiak';
$string['canteditdontownplan'] = 'Egitasmo hau ezin duzu editatu zurea ez delako.';
$string['canteditdontowntask'] = 'Lan hau ezin duzu editatu zurea ez delako.';
$string['completed'] = 'Osatuta';
$string['completeddesc'] = 'Markatu lana osatutzat';
$string['completiondate'] = 'Osaketa-data';
$string['deleteplan'] = 'Ezabatu egitasmoa';
$string['deleteplanconfirm'] = 'Ziur al zaude egitasmo hau ezabatu nahi duzula? Egitasmoa ezabatuz gero, barne dituen lanak ere ezabatuko dira.';
$string['deletetask'] = 'Ezabatu lana';
$string['deletetaskconfirm'] = 'Ziur al zaude lan hau ezabatu nahi duzula?';
$string['deletethisplan'] = 'Ezabatu egitasmoa: \'%s\'';
$string['deletethistask'] = 'Ezabatu lana: \'%s\'';
$string['description'] = 'Deskribapena';
$string['editingplan'] = 'Egitasmoa editatzen';
$string['editingtask'] = 'Lana editatzen';
$string['editplan'] = 'Editatu egitasmoa';
$string['edittask'] = 'Editatu lana';
$string['managetasks'] = 'Kudeatu lanak';
$string['myplans'] = 'Nire egitasmoak';
$string['mytasks'] = 'Nire lanak';
$string['newplan'] = 'Egitasmo berria';
$string['newtask'] = 'Lan berria';
$string['noplans'] = 'Ez dago erakusteko egitasmorik';
$string['noplansaddone'] = 'Ez da egitasmorik diseinatu.';
$string['notasks'] = 'Ez dago erakusteko lanik. %Gehitu bat%s!';
$string['notasksaddone'] = 'Ez da lanik zehaztu. %sGehitu bat%s!';
$string['plan'] = 'egitasmo';
$string['plandeletedsuccessfully'] = 'Egitasmoa ondo ezabatu da.';
$string['plannotdeletedsuccessfully'] = 'Errorea gertatu da egitasmoa ezabatzean';
$string['plannotsavedsuccessfully'] = 'Errorea gertatu da formulario hau bidaltzea. Mesedez, aztertu markatutako eremuak eta saiatu berriz.';
$string['plans'] = 'egitasmo';
$string['plansavedsuccessfully'] = 'Egitasmoa ondo gorde da.';
$string['planstasks'] = '\'%s\' egitasmoaren lanak';
$string['planstasksdesc'] = 'Gehitu lanak eskuinaldeko botoia erabiliz zure egitasmoa diseinatu ahal izateko.';
$string['saveplan'] = 'Gorde egitasmoa';
$string['savetask'] = 'Gorde lana';
$string['task'] = 'lan';
$string['taskdeletedsuccessfully'] = 'Lana ondo ezabatu da.';
$string['tasks'] = 'lan';
$string['tasksavedsuccessfully'] = 'Lana ondo gorde da.';
$string['title'] = 'Izenburua';
$string['titledesc'] = 'Izenburua erabiliko da lan bakoitza Nire Egitasmoak blokean erakusteko.';
?>
