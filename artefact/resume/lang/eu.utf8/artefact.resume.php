<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['academicgoal'] = 'Helburu akademikoak';
$string['academicskill'] = 'Trebetasun akademikoak';
$string['achievements'] = 'Lorpenak';
$string['backtoresume'] = 'Bueltatu nire CVra';
$string['book'] = 'Liburuak eta argitalpenak';
$string['careergoal'] = 'Karrerako helburuak';
$string['certification'] = 'Ziurtagiriak, egiaztagiriak eta sariak';
$string['citizenship'] = 'Bizilekua';
$string['compositedeleteconfirm'] = 'Ziur al zaude ezabatu egin nahi duzula hau?';
$string['compositedeleted'] = 'Ondo ezabatu da';
$string['compositesaved'] = 'Ondo gorde da';
$string['compositesavefailed'] = 'Ezin izan da gorde';
$string['contactinformation'] = 'Kontakturako informazioa';
$string['contribution'] = 'Ekarpena';
$string['coverletter'] = 'Aurkezpen-gutuna';
$string['current'] = 'Oraingoa';
$string['date'] = 'Data';
$string['dateofbirth'] = 'Jaiotze-data';
$string['description'] = 'Deskribapena';
$string['detailsofyourcontribution'] = 'Ekarpenaren xehetasunak';
$string['educationandemployment'] = 'Hezkuntza eta lan-esperientzia';
$string['educationhistory'] = 'Hezkuntza-ibilbidea';
$string['employer'] = 'Enplegu-emailea';
$string['employeraddress'] = 'Lanpostuaren helbidea';
$string['employmenthistory'] = 'Lan-ibilibidea';
$string['enddate'] = 'Amaiera-data';
$string['female'] = 'Emakumezkoa';
$string['gender'] = 'Generoa';
$string['goalandskillsaved'] = 'Ondo gorde da';
$string['goals'] = 'Helburuak';
$string['institution'] = 'Erakundea';
$string['institutionaddress'] = 'Erakundearen helbidea';
$string['interest'] = 'Interesak';
$string['interests'] = 'Interesak';
$string['introduction'] = 'Sarrera';
$string['jobdescription'] = 'Postuaren deskribapena';
$string['jobtitle'] = 'Lanpostuaren izena';
$string['male'] = 'Gizonezkoa';
$string['maritalstatus'] = 'Egoera zibila';
$string['membership'] = 'Partaidetza profesionalak';
$string['movedown'] = 'Mugitu behera';
$string['moveup'] = 'Mugitu gora';
$string['mygoals'] = 'Nire Helburuak';
$string['myresume'] = 'Nire Curriculum Vitaea';
$string['myskills'] = 'Nire trebetasunak';
$string['personalgoal'] = 'Helburu pertsonalak';
$string['personalinformation'] = 'Informazio pertsonala';
$string['personalskill'] = 'Trebetasun pertsonalak';
$string['placeofbirth'] = 'Jaioterria';
$string['pluginname'] = 'Curriculum Vitae';
$string['position'] = 'Postua';
$string['qualdescription'] = 'Agiriaren deskribapena';
$string['qualification'] = 'Agiria';
$string['qualname'] = 'Agiriaren izena';
$string['qualtype'] = 'Agiri-mota';
$string['resume'] = 'Curriculum Vitaea';
$string['resumeofuser'] = '%s(r)en Curriculum Vitaea';
$string['resumesaved'] = 'Curriculum Vitaea gorde da';
$string['resumesavefailed'] = 'Ezin izan da eguneratu zure CVa';
$string['skills'] = 'Trebetasunak';
$string['startdate'] = 'Hasiera-data';
$string['title'] = 'Izenburua';
$string['viewyourresume'] = 'Ikusi Curriculum Vitaea';
$string['visastatus'] = 'Bisatuaren egoera';
$string['workskill'] = 'Lan-trebetasunak';
?>
