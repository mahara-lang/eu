<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['absolutebottom'] = 'Guztiz behean';
$string['absolutemiddle'] = 'Guztiz erdian';
$string['addblog'] = 'Sortu egunkaria';
$string['addone'] = 'Gehitu bat!';
$string['addpost'] = 'Gehitu sarrera';
$string['alignment'] = 'Lerrokadura';
$string['allowcommentsonpost'] = 'Baimendu iruzkinak egitea zure sarrerari.';
$string['allposts'] = 'Sarrera guztiak';
$string['alt'] = 'Deskribapena';
$string['attach'] = 'Erantsi';
$string['attachedfilelistloaded'] = 'Erantsitako fitxategia kargatu da';
$string['attachedfiles'] = 'Erantsitako fitxategiak';
$string['attachment'] = 'Eranskina';
$string['attachments'] = 'Eranskinak';
$string['baseline'] = 'Oinarri-lerroa';
$string['blog'] = 'Egunkaria';
$string['blogcopiedfromanotherview'] = 'Oharra: bloke hau beste orri batetik kopiatu da. Mugitu egin dezakezu edo kendu, baina ezin duzu aldatu %s bertan dagoena.';
$string['blogdeleted'] = 'Egunkaria ezabatu da';
$string['blogdesc'] = 'Deskribapena';
$string['blogdescdesc'] = 'adb., ‘Mikelen esperientzia eta hausnarketen erregistroa’.';
$string['blogdoesnotexist'] = 'Sartu nahian zabiltzan egunkaria ez da existitzen';
$string['blogfilesdirdescription'] = 'Fitxategiak blog-mezuaren eranskin gisa igo dira';
$string['blogfilesdirname'] = 'blog-fitxategiak';
$string['blogpost'] = 'Egunkari-sarrera';
$string['blogpostdeleted'] = 'Egunkari-sarrera ezabatu da';
$string['blogpostdoesnotexist'] = 'Sartu nahian zabiltzen blog-sarrera ez da existitzen';
$string['blogpostpublished'] = 'Egunkari-sarrera ez da argitaratu';
$string['blogpostsaved'] = 'Egunkari-sarrera gorde da';
$string['blogs'] = 'Egunkariak';
$string['blogsettings'] = 'Egunkari-ezarpenak';
$string['blogtitle'] = 'Izenburua';
$string['blogtitledesc'] = 'adb., ‘Mikelen erizaintza-practicumaren egunerokoa’.';
$string['border'] = 'Ertza';
$string['bottom'] = 'Behealdea';
$string['browsemyfiles'] = 'Begiratu nire fitxategiak';
$string['cancel'] = 'Ezeztatu';
$string['cannotdeleteblogpost'] = 'Ezin izan da egunakri-sarrera ezabatu';
$string['commentsallowed'] = 'Iruzkinak';
$string['commentsallowedno'] = 'Blog honetan ez dira iruzkinak onartzen';
$string['commentsallowedyes'] = 'Utzi iruzkinak egiten blog honetan erabiltzaile gisa erregistratutakoei';
$string['commentsnotify'] = 'Iruzkin-oharra';
$string['commentsnotifydesc'] = 'Nahi baduzu, ohar bat jaso dezakezu norbaitek zure blogeko mezu bati erantzuten dion bakoitzean.';
$string['commentsnotifyno'] = 'Ez dut blog honi egindako iruzkinen oharrik jaso nahi';
$string['commentsnotifyyes'] = 'Blog honi egindako iruzkinen oharra jaso nahi dut';
$string['copyfull'] = 'Besteek zure %s-(r)en kopia izango dute';
$string['copynocopy'] = 'Utzi alde batera bloke hau orria kopiatzean';
$string['copyreference'] = 'Besteek zure %s erakutsi dezakete beren orrietan';
$string['createandpublishdesc'] = 'Egunkari-sarrera bat sortuko da eta besteen eskura jarriko da.';
$string['createasdraftdesc'] = 'Egunkari-sarrera sortuko da, baina ez da besteen eskura egongo zuk argitaratzea erabakitzen duzun arte.';
$string['createblog'] = 'Sortu egunkaria';
$string['dataimportedfrom'] = 'Datuak %s(e)tik inportatu dira';
$string['defaultblogtitle'] = '%s(r)en egunkaria';
$string['delete'] = 'Ezabatu';
$string['deleteblog?'] = 'Ziur al zaude blog hau ezabatu nahi duzula?';
$string['deleteblogpost?'] = 'Ziur al zaude sarrera hau ezabatu nahi duzula?';
$string['description'] = 'Deskribapena';
$string['dimensions'] = 'Neurriak';
$string['draft'] = 'Zirriborroa';
$string['edit'] = 'Editatu';
$string['editblogpost'] = 'Editatu egunkari-sarrera';
$string['enablemultipleblogstext'] = 'Egunkari bat duzu. Beste egunkari bat nahi bazenu, gaitu egunkari anitzetarako aukera <a href="%saccount/">kontuaren ezarpenak</a> orrian.';
$string['entriesimportedfromleapexport'] = 'LEAP esportazio batetik inportatutako sarrerak, beste inondik inportatu ezin izan zirenak';
$string['erroraccessingblogfilesfolder'] = 'Errorea blog-fitxategien karpetara sartzean';
$string['errorsavingattachments'] = 'Errorea gertatu da egunkari-sarreraren eranskinak gordetzean';
$string['feedrights'] = '%s Copyright-a.';
$string['feedsnotavailable'] = 'Tresna-mota honetarako jarioak ezin dira erabili.';
$string['horizontalspace'] = 'Espazio horizontala';
$string['image_list'] = 'Erantsitako irudia';
$string['insert'] = 'Txertatu';
$string['insertimage'] = 'Txertatu irudia';
$string['left'] = 'Ezkerra';
$string['middle'] = 'Erdia';
$string['moreoptions'] = 'Aukera gehiago';
$string['mustspecifycontent'] = 'Zehaztu egin behar duzu sarreraren edukia';
$string['mustspecifytitle'] = 'Izenburua jarri behar diozu sarrerari';
$string['myblog'] = 'Nire bloga';
$string['myblogs'] = 'Nire blogak';
$string['name'] = 'Izena';
$string['newattachmentsexceedquota'] = 'Sarrera honetara igo dituzun fitxategien tamainak zure kuota gaindituko du. Sarrera gorde ahal izateko, ezabatu egin behar dituzu gehitu berri dituzun fitxategi batzuk.';
$string['newblog'] = 'Egunkari berria';
$string['newblogpost'] = 'Egunkari-sarrera berria "%s" egunkarian';
$string['newerposts'] = 'Sarrera berriak';
$string['nofilesattachedtothispost'] = 'Ez da fitxategirik erantsi';
$string['noimageshavebeenattachedtothispost'] = 'Ez da irudirik erantsi sarrera honetan. Irudi bat igo edo erantsi behar diozu sarrerari txertatu aurretik.';
$string['nopostsaddone'] = 'Mezurik ez oraindik. %sGehitu bat%s!';
$string['nopostsyet'] = 'Ez dago sarrerarik oraindik.';
$string['noresults'] = 'Ez da egunkari-sarrerarik aurkitu';
$string['olderposts'] = 'Sarrera zaharrak';
$string['pluginname'] = 'Egunkariak';
$string['post'] = 'sarrera';
$string['postbody'] = 'Gorputza';
$string['postedbyon'] = '%s(e)k bidalitakoa %s-(e)n';
$string['postedon'] = 'Noiz bidalia:';
$string['posts'] = 'sarrera';
$string['postscopiedfromview'] = '%s(e)tik kopiatutako sarrerak';
$string['posttitle'] = 'Izenburua';
$string['publish'] = 'Argitaratu';
$string['publishblogpost?'] = 'Ziur al zaude sarrera hau argitaratu nahi duzula?';
$string['published'] = 'Argitaratuta';
$string['publishfailed'] = 'Errorea gertatu da.  Zure sarrera ez da argitaratu';
$string['remove'] = 'Ezabatu';
$string['right'] = 'Eskuina';
$string['save'] = 'Gorde';
$string['saveandpublish'] = 'Gorde eta argitaratu';
$string['saveasdraft'] = 'Gorde zirriborro gisa';
$string['savepost'] = 'Gorde sarrera';
$string['savesettings'] = 'Gorde ezarpenak';
$string['settings'] = 'Ezarpenak';
$string['src'] = 'Irudiaren URLa';
$string['textbottom'] = 'Testu-botoia';
$string['texttop'] = 'Goialdeko testua';
$string['thisisdraft'] = 'Sarrera hau zirriborro bat da';
$string['thisisdraftdesc'] = 'Sarreraren zirriborroa zeuk bakarrik ikus dezakezu.';
$string['title'] = 'Izenburua';
$string['top'] = 'Gora';
$string['update'] = 'Igora';
$string['verticalspace'] = 'Espazio bertikala';
$string['viewblog'] = 'Egunkari-bista';
$string['viewposts'] = 'Kopiatutako sarrerak (%s)';
$string['youarenottheownerofthisblog'] = 'Ez zara egunkari honen jabea';
$string['youarenottheownerofthisblogpost'] = 'Ez zara egunkari-sarrera honen jabea';
$string['youhaveblogs'] = '%s egunkari dituzu.';
$string['youhavenoblogs'] = 'Ez duzu egunkaririk.';
$string['youhaveoneblog'] = 'Egunkari bat duzu.';
?>
