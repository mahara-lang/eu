<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['Allow'] = 'Baimendu';
$string['Attachments'] = 'Eranskinak';
$string['Comment'] = 'Iruzkina';
$string['Comments'] = 'Iruzkinak';
$string['Moderate'] = 'Moderatu';
$string['allowcomments'] = 'Baimendu iruzkinak';
$string['approvalrequired'] = 'Iruzkinak moderatu egiten dira, beraz iruzkin hau publiko egitea aukeratzen baduzu ezin izango du inork ikusi, jabeak onartu arte.';
$string['artefactdefaultpermissions'] = 'Lehenetsitako iruzkin-baimena';
$string['artefactdefaultpermissionsdescription'] = 'Aukeratutako tresna motek sortzen direnetik iruzkinak aktibatuta izango dituzte. Erabiltzaileek ezarpen horiek gaindi ditzakete kasu partikularretan.';
$string['attachfile'] = 'Erantsitako fitxategia';
$string['cantedithasreplies'] = 'Azken iruzkina bakarrik edita dezakezu';
$string['canteditnotauthor'] = 'Zu ez zara iruzkin honen egilea';
$string['cantedittooold'] = 'Duela %d minutu baino lehenagoko iruzkinak ezin dituzu editatu';
$string['comment'] = 'iruzkin';
$string['commentdeletedauthornotification'] = 'Zure iruzkina %s(e)n ezabatu da: %s';
$string['commentdeletednotificationsubject'] = '%s(e)ko iruzkina ezabatuta';
$string['commentmadepublic'] = 'Iruzkin publikoa';
$string['commentnotinview'] = '%d iruzkina ez dago %d orrian';
$string['commentratings'] = 'Gaitu iruzkinen kalifikazioa';
$string['commentremoved'] = 'Iruzkina ezabatu da';
$string['commentremovedbyadmin'] = 'Kudeatzaile batek ezabatu du iruzkina';
$string['commentremovedbyauthor'] = 'Egileak ezabatutako iruzkina';
$string['commentremovedbyowner'] = 'Jabeak ezabatutako iruzkina';
$string['comments'] = 'iruzkin';
$string['commentupdated'] = 'Iruzkina eguneratu da';
$string['editcomment'] = 'Editatu iruzkina';
$string['editcommentdescription'] = 'Zure iruzkinak duela %d minutu baino gutxiago egin badira eta  erantzunik ez badutu, egunera ditzakezu. Minutu horiek igarota duzun aukera bakarra iruzkinak ezabatu eta berriak egitea da.';
$string['entriesimportedfromleapexport'] = 'LEAP-etik inportatutako sarrerak, beste nonbaitetik ezin izan direnak inportatu';
$string['feedback'] = 'Feedbacka';
$string['feedbackattachdirdesc'] = 'Iruzkinei erantsitako fitxategiak zure portfolioan';
$string['feedbackattachdirname'] = 'iruzkinen fitxategiak';
$string['feedbackattachmessage'] = 'Erantsitako fitxategia(k) zure %s karpetan gehitu d(ir)a';
$string['feedbackdeletedhtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;"><strong>%s-ko iruzkin bat ezabatu egin da</strong><br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p><a href="%s">%s</a></p> </div>';
$string['feedbackdeletedtext'] = '%s-ko iruzkin bat kendu egin da %s ------------------------------------------------------------------------ %s ------------------------------------------------------------------------ %s on-line ikusteko erabili esteka hau: %s';
$string['feedbacknotificationhtml'] = '<div style="padding: 0.5em 0; border-bottom: 1px solid #999;"><strong>%s iruzkindua hemen: %s</strong><br>%s</div> <div style="margin: 1em 0;">%s</div> <div style="font-size: smaller; border-top: 1px solid #999;"> <p><a href="%s">Erantzun iruzkin honi on-line</a></p> </div>';
$string['feedbacknotificationtext'] = '%s iruzkindua hemen: %s %s ------------------------------------------------------------------------ %s ------------------------------------------------------------------------ Iruzkina ikusi eta on-line erantzuteko, erabili esteka hau: %s';
$string['feedbackonviewbyuser'] = '%s(e)k emandako feedbacka %s(e)n';
$string['feedbacksubmitted'] = 'Feedbacka bidalita';
$string['lastcomment'] = 'Azken iruzkina';
$string['makepublic'] = 'Publiko egin';
$string['makepublicnotallowed'] = 'Ez duzu iruzkin hau publiko egiteko baimenik';
$string['makepublicrequestbyauthormessage'] = '%s(e)k bere iruzkina publiko egiteko eskatu dizu.';
$string['makepublicrequestbyownermessage'] = '%s(e)k zure iruzkina publiko egiteko eskatu dizu.';
$string['makepublicrequestsent'] = '%s-ri mezua bidali zaio iruzkina publiko egiteko eskatuz.';
$string['makepublicrequestsubject'] = 'Mezu pribatua publiko egiteko eskaria';
$string['messageempty'] = 'Mezua hutsik dago. Mezu hutsa bakarrik onartzen da fitxategi bat eransten baduzu.';
$string['moderatecomments'] = 'Moderatu iruzkinak';
$string['moderatecommentsdescription'] = 'Iruzkinak pribatuak izango dira zuk onartu arte.';
$string['newfeedbacknotificationsubject'] = 'Feedback berria %s(e)n';
$string['placefeedback'] = 'Eman feedbacka';
$string['pluginname'] = 'Iruzkina';
$string['rating'] = 'Kalifikazioa';
$string['reallydeletethiscomment'] = 'Ziur al zaude iruzkin hau ezabatu nahi duzula?';
$string['thiscommentisprivate'] = 'Iruzkin hau pribatua da';
$string['typefeedback'] = 'Feedbacka';
$string['viewcomment'] = 'Ikusi iruzkina';
$string['youhaverequestedpublic'] = 'Iruzkin hau publiko egiteko eskatu duzu.';
?>
