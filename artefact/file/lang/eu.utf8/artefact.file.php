<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['Contents'] = 'Edukiak';
$string['Continue'] = 'Jarraitu';
$string['Created'] = 'Sortuta';
$string['Date'] = 'Data';
$string['Default'] = 'Lehenetsia';
$string['Description'] = 'Deskribapena';
$string['Details'] = 'Xehetasunak';
$string['Download'] = 'Jaitsi';
$string['File'] = 'Fitxategia';
$string['Files'] = 'Fitxategiak';
$string['Folder'] = 'Direktorioa';
$string['Folders'] = 'Direktorioak';
$string['Images'] = 'Irudiak';
$string['Name'] = 'Izena';
$string['Owner'] = 'Jabea';
$string['Preview'] = 'Aurreikuspena';
$string['Size'] = 'Tamaina';
$string['Title'] = 'Titulua';
$string['Type'] = 'Mota';
$string['Unzip'] = 'Unzip';
$string['addafile'] = 'Gehitu fitxategia';
$string['ai'] = 'Postscript dokumentua';
$string['aiff'] = 'AIFF audio-fitxategia';
$string['application'] = 'Aplikazio ezezaguna';
$string['archive'] = 'Gorde';
$string['au'] = 'AU audio-fitxategia';
$string['avi'] = 'AVI bideo-fitxategia';
$string['bmp'] = 'Bitmap irudia';
$string['bytes'] = 'byteak';
$string['bz2'] = 'Bzip2 konprimitutako fitxategia';
$string['cannoteditfolder'] = 'Ez duzu karpeta honi edukia gehitzeko baimenik';
$string['cannoteditfoldersubmitted'] = 'Ezin duzu direktori batera edukia gehitu bidalitako orri batean.';
$string['cannotremovefromsubmittedfolder'] = 'Ezin duzu direktorio baten edukia ezabatu bidalitako orri batean.';
$string['cantcreatetempprofileiconfile'] = 'Ezin da momentuz profil-ikonoa %s profilean idatzi';
$string['changessaved'] = 'Aldaketak gorde dira';
$string['clickanddragtomovefile'] = 'Sakatu eta eraman %s mugitzeko';
$string['confirmdeletefile'] = 'Ziur al zaude fitxategi hau ezabatu nahi duzula?';
$string['confirmdeletefolder'] = 'Ziur al zaude direktorio hau ezabatu nahi duzula?';
$string['confirmdeletefolderandcontents'] = 'Ziur al zaude direktorio hau eta dituen eduki guztiak ezabatu nahi dituzula?';
$string['contents'] = 'Edukiak';
$string['copyrightnotice'] = 'Copyright-oharra';
$string['create'] = 'Sortu';
$string['createfolder'] = 'Sortu karpeta';
$string['customagreement'] = 'Pertsonalizatutako akordioa';
$string['defaultagreement'] = 'Lehenetsitako akordioa';
$string['defaultquota'] = 'Lehenetsitako kuota';
$string['defaultquotadescription'] = 'Nahi izanez gero, aukera duzu zehazteko zenbateko kuota izango duten diskotik erabiltzaile berriek. Oraingo erabiltzaileen kuota ez da aldatuko.';
$string['deletefile?'] = 'Ziur al zaude fitxategi hau ezabatu nahi duzula?';
$string['deletefolder?'] = 'Ziur al zaude karpeta hau ezabatu nahi duzula?';
$string['deleteselectedicons'] = 'Ezabatu aukeratutako irudiak';
$string['deletingfailed'] = 'Ezin izan da ezabatu: fitxategia edo direktoria ez da existitzen inon';
$string['destination'] = 'Helbidea';
$string['doc'] = 'MS Word dokumentua';
$string['downloadfile'] = 'Jaitsi %s';
$string['downloadoriginalversion'] = 'Jaitsi jatorrizko bertsioa';
$string['dss'] = 'DSS soinu-fitxategia';
$string['editfile'] = 'Editatu fitxategia';
$string['editfolder'] = 'Editatu karpeta';
$string['editingfailed'] = 'Ezin izan da editatu: fitxategia edo direktoria ez da existitzen inon';
$string['emptyfolder'] = 'Karpeta hutsa';
$string['extractfilessuccess'] = '%s direktorio eta %s fitxategi sortuta.';
$string['file'] = 'Fitxategia';
$string['fileadded'] = 'Fitxategia aukeratuta';
$string['filealreadyindestination'] = 'Mugitutako fitxategia karpeta horretan dago jada';
$string['fileappearsinviews'] = 'Fitxategia zure orri batean edo gehiagotan agertzen da.';
$string['fileattached'] = 'Fitxategia beste %s elementuri erantsita dago zure portfolioan.';
$string['fileexists'] = 'Fitxategia badago';
$string['fileexistsonserver'] = 'Badago %s izeneko beste fitxategi bat.';
$string['fileexistsoverwritecancel'] = 'Badago fitxategi bat izen horrekin.  Beste izen batekin saiatu zaitezke, edo ordezkatu aurreko fitxategia.';
$string['fileinstructions'] = 'Igo irudiak, dokumentuak edo bestelako fitxategiak bistetan txertatzeko. Fitxategi edo karpeta bat mugitzeko, arrastatu karpeta batera.';
$string['filelistloaded'] = 'Fitxategi-zerrenda kargatu da';
$string['filemoved'] = 'Fitxategia ondo mugitu da';
$string['filenamefieldisrequired'] = 'Fitxategi-eremua bete behar da';
$string['filepermission.edit'] = 'Editatu';
$string['filepermission.republish'] = 'Argitaratu';
$string['filepermission.view'] = 'Ikusi';
$string['fileremoved'] = 'Fitxategia ezabatu da';
$string['files'] = 'fitxategiak';
$string['filesextractedfromarchive'] = 'Artxibotik ateratako fitxategiak';
$string['filesextractedfromziparchive'] = 'Zip fitxategitikateratako fitxategiak';
$string['fileswillbeextractedintofolder'] = 'Fitxategiak hemendik aterako dira: %s';
$string['filethingdeleted'] = '%s ezabatuta';
$string['fileuploadedas'] = '%s eguneratuta "%s" gisa';
$string['fileuploadedtofolderas'] = '%s(e)k eguneratuta %s(r)a "%s" gisa';
$string['filewithnameexists'] = '&quot;%s&quot; izeneko fitxategi edo direktorioa badago dagoeneko.';
$string['flv'] = 'FLV Flash pelikula';
$string['folder'] = 'Direktorioa';
$string['folderappearsinviews'] = 'Direktorio hau orri batean edo gehiagotan agertzen da.';
$string['foldercreated'] = 'Direktorioa sortu da';
$string['foldernamerequired'] = 'Mesedez, eman izena direktorioari.';
$string['foldernotempty'] = 'Direktorio hau ez dago hutsik.';
$string['gif'] = 'GIF irudia';
$string['gotofolder'] = 'Joan hona: %s';
$string['groupfiles'] = 'Taldeko fitxategiak';
$string['gz'] = 'Gzip konprimituriko fitxategia';
$string['home'] = 'Hasiera';
$string['html'] = 'HTML fitxategia';
$string['htmlremovedmessage'] = '&lt;a href=&quot;%s&quot;&gt;%s&lt;/a&gt;-(r)en &lt;strong&gt;%s&lt;/strong&gt; ikusten ari zara. Behean ageri den fitxategia iragazi egin da eduki arriskutsua ezabatzeko, eta jatorrizkoaren gutxi gorabeherako irudikapena baino ez da.';
$string['htmlremovedmessagenoowner'] = '<strong>%s</strong> ikusten ari zara. Behean ageri den fitxategia iragazi egin da eduki arriskutsua ezabatzeko, eta jatorrizkoaren gutxi gorabeherako irudikapena baino ez da.';
$string['image'] = 'Irudia';
$string['imagetitle'] = 'Irudiaren izenburua';
$string['insufficientquotaforunzip'] = 'Geratzen zaizun fitxategi-kuota txikiegia da fitxategi hau konprimitzeko.';
$string['invalidarchive'] = 'Errorea artxiboko fitxategia irakurtzean.';
$string['jpeg'] = 'JPEG irudia';
$string['jpg'] = 'JPG irudia';
$string['js'] = 'Javascript fitxategia';
$string['lastmodified'] = 'Azken aldaketa';
$string['latex'] = 'LaTeX dokumentua';
$string['m3u'] = 'M3U audio-fitxategia';
$string['maxquota'] = 'Gehienezko kuota';
$string['maxquotadescription'] = 'Kudeatzaile batek erabiltzaile bati eman diezaiokeen gehienezko kuota ezar dezakezu. Honek ez die erangingo dagoeneko dauden erabiltzaileen kuotei.';
$string['maxquotaenabled'] = 'Guneko gehienezko kuota behartu';
$string['maxquotaexceeded'] = 'Ezarri duzun kuotak gune honetarako gehienez ezarrita dagoena (%s) gainditzen du. Saiatu txikiago bat ezartzen edo jarri harremanetan gunearen kudeatzailearekin gehinezko kuota handi dezan.';
$string['maxquotaexceededform'] = 'Mesedez, ezarri %s baino kuota txikiagoa.';
$string['maxquotatoolow'] = 'Gehinezko kuotak ezin du berezko kuota baino txikiagoa izan.';
$string['maxuploadsize'] = 'Igotzeko gehienezko tamaina';
$string['mov'] = 'MOV Quicktime pelikula';
$string['movefailed'] = 'Ezin izan da mugitu';
$string['movefaileddestinationinartefact'] = 'Ezin duzu karpeta bat bere baitan gorde.';
$string['movefaileddestinationnotfolder'] = 'Karpetetara fitxategiak baino ezin dituzu mugitu.';
$string['movefailednotfileartefact'] = 'Bakarrik mugi daitezke fitxategiak, karpetak eta irudiak.';
$string['movefailednotowner'] = 'Ez duzu baimenik fitxategia karpeta horretan mugitzeko';
$string['movingfailed'] = 'Ezin izan da mugitu: fitxategia edo direktoria ez da existitzen inon';
$string['mp3'] = 'MP3 audio-fitxategia';
$string['mp4_audio'] = 'MP4 audio-fitxategia';
$string['mp4_video'] = 'MP4 bideo-fitxategia';
$string['mpeg'] = 'MPEG pelikula';
$string['mpg'] = 'MPG pelikula';
$string['myfiles'] = 'Nire fitxategiak';
$string['namefieldisrequired'] = 'Izenaren eremua bete behar da';
$string['nametoolong'] = 'Izena luzeegia da. Mesedez, sartu izen laburragoa.';
$string['nofilesfound'] = 'Ez da fitxategirik aurkitu';
$string['noimagesfound'] = 'Ez da irudirik aurkitu';
$string['notpublishable'] = 'Ez duzu fitxategi hau argitaratzeko baimenik.';
$string['odb'] = 'Openoffice datu-basea';
$string['odc'] = 'Openoffice kalkulu-fitxategia';
$string['odf'] = 'Openoffice formula-fitxategia';
$string['odg'] = 'Openoffice grafiko-fitxategia';
$string['odi'] = 'Openoffice irudia';
$string['odm'] = 'Openoffice Master dokumentu-fitxategia';
$string['odp'] = 'Openoffice aurkezpena';
$string['ods'] = 'Openoffice kalkulu-orria';
$string['odt'] = 'Openoffice dokumentua';
$string['onlyfiveprofileicons'] = 'Ezin duzu bost profil-iruditik gora igo';
$string['or'] = 'edo';
$string['oth'] = 'Openoffice web-dokumentua';
$string['ott'] = 'Openoffice txantiloi-dokumentua';
$string['overwrite'] = 'Ordezkatu';
$string['parentfolder'] = 'Goragoko karpeta';
$string['pdf'] = 'PDF dokumentua';
$string['pleasewaitwhileyourfilesarebeingunzipped'] = 'Mesedez, itxaron zure fitxategiak deskonprimitu arte.';
$string['pluginname'] = 'Fitxategiak';
$string['png'] = 'PNG irudia';
$string['ppt'] = 'MS Powerpoint dokumentua';
$string['profileicon'] = 'Profil-irudia';
$string['profileiconimagetoobig'] = 'Igo duzun irudia handiegia da (%sx%s pixel). Ezin du izan %sx%s pixeletik gorakoa.';
$string['profileicons'] = 'Profil-irudiak';
$string['profileiconsdefaultsetsuccessfully'] = 'Lehenetsitako profil-irudia ondo ezarri da.';
$string['profileiconsdeletedsuccessfully'] = 'Profil-irudia(k) ondo ezabatu d(ir)a';
$string['profileiconsetdefaultnotvalid'] = 'Ezin izan da lehenetsitako profil-irudia ezarri; aukeratutakoak ez du balio.';
$string['profileiconsiconsizenotice'] = 'Gehienez <strong>bost</strong> profil-irudi igo ditzakezu hona, eta bat aukeratu lehenetsitako profil-irudi gisa ager dadin. Irudiek 16x16-ko neurria izan behar dute eta %sx%s pixel, gehienez.';
$string['profileiconsize'] = 'Profil-irudiaren tamaina';
$string['profileiconsnoneselected'] = 'Ez da profil-irudirik aukeratu ezabatzeko';
$string['profileiconuploadexceedsquota'] = 'Profil-irudi hau igoz gero, diskoan duzun kuota gaindituko zenuke. Saiatu igota dituzun fitxategi batzuk ezabatzen.';
$string['quicktime'] = 'Quicktime pelikula';
$string['ra'] = 'Real Audio fitxategia';
$string['ram'] = 'RAM Real Player pelikula';
$string['removingfailed'] = 'Ezin izan da mugitu: fitxategia edo direktoria ez da existitzen inon';
$string['requireagreement'] = 'Akordioa behar du';
$string['rm'] = 'RM Real Player pelikula';
$string['rpm'] = 'RPM Real Player pelikula';
$string['rtf'] = 'RTF dokumentua';
$string['savechanges'] = 'Gorde aldaketak';
$string['selectafile'] = 'Aukera ezazu fitxategi bat';
$string['selectingfailed'] = 'Ezin izan da aukeratu: fitxategia edo direktoria ez da existitzen inon';
$string['setdefault'] = 'Ezarri lehenespena';
$string['sgi_movie'] = 'SGI pelikula-fitxategia';
$string['sh'] = 'Shell Script';
$string['sitefilesloaded'] = 'Guneko fitxategiak kargatuta';
$string['spacerequired'] = 'Beharrezko espazioa';
$string['spaceused'] = 'Espazioa erabili da';
$string['swf'] = 'SWF Flash pelikula';
$string['tar'] = 'TAR fitxategia';
$string['timeouterror'] = 'Ezin izan da fitxategia igo; saiatu berriro.';
$string['title'] = 'Izena';
$string['titlefieldisrequired'] = 'Izenaren eremua bete behar da';
$string['txt'] = 'Testu lauko fitxategia';
$string['unlinkthisfilefromblogposts?'] = 'Fitxategi hau blog-mezu bati edo gehiagori dago atxikita. Fitxategia ezabatzen baduzu, desagertu egingo da mezu horietatik.';
$string['unzipprogress'] = '%s fitxategiak/direktorioak sortuta.';
$string['upload'] = 'Igo';
$string['uploadagreement'] = 'Igo akordioa';
$string['uploadagreementdescription'] = 'Gaitu aukera hau fitxategi bat igo aurretik erabiltzaileak ondorengo testua onartzera behartu nahi badituzu.';
$string['uploadedprofileiconsuccessfully'] = 'Profil-irudi berria ondo igo da';
$string['uploadexceedsquota'] = 'Fitxategi hau igoz gero, diskoan duzun kuota gaindituko zenuke. Saiatu igota dituzun fitxategi batzuk ezabatzen.';
$string['uploadfile'] = 'Igo fitxategia';
$string['uploadfileexistsoverwritecancel'] = 'Badago fitxategi bat izen horrekin.  Beste izen batekin saiatu zaitezke, edo ordezkatu aurreko fitxategia.';
$string['uploadingfile'] = 'fitxategia igotzen...';
$string['uploadingfiletofolder'] = '%s %s-(e)ra igotzen ari da';
$string['uploadoffilecomplete'] = '%s ondo igo da';
$string['uploadoffilefailed'] = 'Ezin izan da %s igo';
$string['uploadoffiletofoldercomplete'] = '%s ondo igo da %s(e)ra';
$string['uploadoffiletofolderfailed'] = '%s ezin izan da %s(e)ra igo';
$string['uploadprofileicon'] = 'Igo profil-irudia';
$string['usecustomagreement'] = 'Erabili pertsonalizatutako akordioa';
$string['usenodefault'] = 'Ez erabili lehenespenik';
$string['usingnodefaultprofileicon'] = 'Ez zara lehenetsitako profil-irudirik erabiltzen ari';
$string['wav'] = 'WAV audio-fitxategia';
$string['wmv'] = 'WMV bideo-fitxategia';
$string['wrongfiletypeforblock'] = 'Igo duzun fitxategia ez da bloke honetarako behar de motakoa.';
$string['xml'] = 'XML fitxategia';
$string['youmustagreetothecopyrightnotice'] = 'Ados egon behar duzu copyright oharrarekin';
$string['zip'] = 'ZIP fitxategia';
?>
