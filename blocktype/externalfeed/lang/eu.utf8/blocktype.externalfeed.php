<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['defaulttitledescription'] = 'Zuri utziz gero, jarioaren izenburua erabiliko da';
$string['description'] = 'Enbotatu kanpoko RSS edo ATOM jarioa';
$string['feedlocation'] = 'Jarioaren kokapena';
$string['feedlocationdesc'] = 'RSS edo ATOM jario baliagarri baten URLa';
$string['invalidfeed'] = 'Jario okerra dela dirudi. Jakinarazitako errorea ondokoa da: %s';
$string['invalidurl'] = 'URL hori okerra da. Jarioak http edo https URLen bidez ikus ditzakezu soilik.';
$string['itemstoshow'] = 'Erakusteko elementuak';
$string['itemstoshowdescription'] = '1 eta 20 bitartean';
$string['lastupdatedon'] = '%s eguneratu zen azkenengoz';
$string['showfeeditemsinfull'] = 'Jarioaren elementuak osorik erakutsi?';
$string['showfeeditemsinfulldesc'] = 'Jarioaren elementuen aurkibidea erakutsi soilik edo bakoitzaren deskribapena ere bai';
$string['title'] = 'Kanpoko jarioa';
?>
