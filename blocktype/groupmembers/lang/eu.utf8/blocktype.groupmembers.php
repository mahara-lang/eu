<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['Latest'] = 'Azkena';
$string['Random'] = 'Ausaz';
$string['defaulttitledescription'] = 'Eremu hau zuriz uzten baduzu, lehenetsitako izenburu bat berez sortuko da.';
$string['description'] = 'Talde honetako partaideen zerrenda erakutsi';
$string['options_columns_desc'] = 'Erakutsi nahi duzun zutabe-kopurua';
$string['options_columns_title'] = 'Zutabeak';
$string['options_numtoshow_desc'] = 'Ikusi nahi duzun partaide-kopurua';
$string['options_numtoshow_title'] = 'Erakutsitako partaideak';
$string['options_order_desc'] = 'Aukeran duzu taldeko azken partaideak erakutsi ala ausazko aukeraketa bat.';
$string['options_order_title'] = 'Ordena';
$string['options_rows_desc'] = 'Erakutsi nahi duzun errenkada-kopurua';
$string['options_rows_title'] = 'Errenkadak';
$string['show_all'] = 'Ikusi talde honetako partaide guztiak...';
$string['title'] = 'Taldeko partaideak';
?>
