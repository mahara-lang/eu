<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['Post'] = 'Bidali';
$string['addpostsuccess'] = 'Mezua arrakastaz gehitu da';
$string['backtoprofile'] = 'Itzuli profilera';
$string['delete'] = 'Ezabatu mezua';
$string['deletepost'] = 'Ezabatu mezua';
$string['deletepostsuccess'] = 'Mezua ondo ezabatu da';
$string['deletepostsure'] = 'Ziur al zaude hau ezabatu nahi duzula? Ezin da berregin.';
$string['description'] = 'Erakutsi gune bat jendeak iruzkinak egin ahal izateko';
$string['makeyourpostprivate'] = 'Zure mezua pribatu egin nahi duzu?';
$string['maxcharacters'] = 'Mezuko gehienez %s karaktere.';
$string['noposts'] = 'Ez dago erakusteko mezurik';
$string['otherusertitle'] = '%s(r)en horma';
$string['postsizelimit'] = 'Mezuaren gehienezko tamaina';
$string['postsizelimitdescription'] = 'Hemen, mezuen gehienezko tamaina ezar dezakezu. Orain arteko mezuak ez dira aldatuko.';
$string['postsizelimitinvalid'] = 'Zenbaki horrek ez du balio';
$string['postsizelimitmaxcharacters'] = 'Gehienezko karaktere-muga';
$string['postsizelimittoosmall'] = 'Muga hori ezin da izan zero baino txikiagoa.';
$string['posttextrequired'] = 'Eremu hau nahitaezkoa da.';
$string['reply'] = 'errepikatu';
$string['sorrymaxcharacters'] = 'Sentitzen dugu, mezuak ezin du %s karaktere baino gehiago izan.';
$string['title'] = 'Horma';
$string['viewwall'] = 'Ikusi horma';
$string['wall'] = 'Horma';
$string['wholewall'] = 'Ikusi horma osoa';
?>
