<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/eu.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Mahara Basque Translation Team (current and past members):
 *                - Santurtziko Udal Euskaltegia (www.santurtzieus.com)
 *                - Mondragon Unibertsitatea (www.mondragon.edu)
 *                - Tknika (www.tknika.net)
 *                - i2basque (www-eu.i2basque.es)
 * @copyright  Copyright (C) 2009 onwards Mahara Basque Translation Team
 *
 */

defined('INTERNAL') || die();

$string['alttext'] = 'Creative Commons Lizentzia';
$string['blockcontent'] = 'Blokearen edukia';
$string['by'] = 'Eskubidea';
$string['by-nc'] = 'CC - EzKomertziala';
$string['by-nc-nd'] = 'CC - Aitortu-EzKomertziala-LanEratorririkGabe';
$string['by-nc-sa'] = 'CC - Aitortu-EzKomertziala-PartekatuBerdin';
$string['by-nd'] = 'CC - Aitortu-LanEratorririkGabe';
$string['by-sa'] = 'CC ShareAlike eskubidea';
$string['cclicensename'] = 'Creative Commons %s 3.0 Unported';
$string['cclicensestatement'] = '%s  %s(r)ena  %s lizentziapean dago.';
$string['config:noderivatives'] = 'Zure lanerako aldaketak onartu?';
$string['config:noncommercial'] = 'Zure lanerako merkataritza-erabilera onartu?';
$string['config:sharealike'] = 'Bai, baldin eta gainerakoek berdin partekatzen badute';
$string['description'] = 'Erantsi Creative Commons lizentzia zure orriari';
$string['licensestatement'] = 'Lan hau <a rel="license" href="%s">Creative Commons %s 3.0 Unported License</a> lizentziapekoa da.';
$string['otherpermissions'] = '%s(e)k eman litzake baimenak lizentzia honetatik haratago.';
$string['sealalttext'] = 'Lizentzia hau Doako Kultur-laneratako onartzen da';
$string['title'] = 'Creative Commons Lizentzia';
?>
